<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
* Socket Connection for TCP/IP Data Generator
* @author Lyon Sun <lyonsun@techtrex.com>
*/
class DataGenerator
{
	// Create Vendor
	public function create_vendor($vendorname, $mkey)
	{
		// check if vendor name is 16 long and return valid ones.
		$vendorname = $this->check_input_length($vendorname, 16);
		
		// prefix and suffix of data string to be passed.
		$prefix 	= "CV";

		// pack Vendor specific ID as Hexadecimal value.
		$vendor_id 	= pack("H*", '000002');

		// length of data to be passed.
		$request_length = strlen($prefix) + strlen($vendorname) + strlen($vendor_id) + strlen($mkey);

		// convert length into hexadecimal.
		$request_length_hex = sprintf("%04x", $request_length);

		// then pack this as hexadecimal value.
		$request_length_binary = pack("H*", $request_length_hex);

		// data to be passed.
		$request = $request_length_binary.$prefix.$vendorname.$vendor_id.$mkey;

		// send request again with response length.
		$response = $this->socket_request_response($request);

		// if response return -1, socket error, return error status.
		if ($response == "-1") {
			return $result_array = array(
				'cmd' 		=> $prefix,
				'status' 	=> 'xxxx',
			);
		}

		// cut off the first two chars that represents length of response.
		$response_length 	= $response['response_length'];

		$response_data		= $response['response_data'];

		// the first two chars represent CMD passed through.
		$cmd 	= substr($response_data, 0, 2);

		// then get status code.
		$status = array_shift(unpack("H*", substr($response_data, 2, 2)));

		// then get result_data.
		$unpack_binary_response 	= array_shift(unpack("H*", substr($response_data, 4)));

		// build an array to pass data through into controller.
		$result_array = array(
			'length' 		=> $response_length-2,
			'cmd'			=> $cmd,
			'status' 		=> $status,
		);

		// if unpack_binary_response not empty, add item into returning array. 
		if (!empty($unpack_binary_response)) {
	    	$result_array['hex_res'] = $unpack_binary_response;
		}
		
		// passing this array if socket succeed.
		return $result_array;
	}

	// Generate License LCMS
	public function generate_lcms($vendorname, $pcmac, $snumber)
	{
		// check if vendor name is 16 long and return valid one.
		$vendorname = $this->check_input_length($vendorname, 16);

		// prefix(CMD) of data string to be passed.
		$prefix 	= "LL";

		// pack Number of Mac Address as Hexadecimal value.
		$num_mac 	= pack("H*", '01');

		// do the same to Mac Address.
		$macAddr 	= pack("H*", $pcmac);

		// check if sequence number is 16 long and return valid one.
		$snumber = $this->check_input_length($snumber, 16);

		// length of data to be passed.
		$request_length = strlen($prefix) + strlen($vendorname) + strlen($num_mac) + strlen($macAddr) + strlen($snumber);

		// convert length into hexadecimal.
		$request_length_hex = sprintf("%04x", $request_length);

		// then pack this as hexadecimal value.
		$request_length_binary = pack("H*", $request_length_hex);

		// data to be passed.
		$request = $request_length_binary.$prefix.$vendorname.$num_mac.$macAddr.$snumber;

		// send request again with response length.
		$response = $this->socket_request_response($request);

		// if response return -1, socket error, return error status.
		if ($response == "-1") {
			return $result_array = array(
				'cmd' 		=> $prefix,
				'status' 	=> 'xxxx',
			);
		}

		// cut off the first two chars that represents length of response.
		$response_length 	= $response['response_length'];

		// content(length): cmd(2)+status(2)+path(44)+space(1)+title(11)+semicolon(1)+length(2)+data(256).
		$response_data		= $response['response_data'];

		$this->write_log_for_response_data($response_data);

		// then get the string without the last 256 chars.
		$header = substr($response_data, 0, -256);

		// the first two chars represent CMD passed through.
		$cmd 	= substr($header, 0, 2);

		// then get status code.
		$status = array_shift(unpack("H*", substr($response_data, 2, 2)));

		// then get address.
		$addr 	= substr($header, 5, -15);

		// trim "License Data" from the header string.
		$title 	= substr($header, -14, -3);

		// the last 256 chars are binary data.
		$binary_response = substr($response_data, -256);

		// convert this binary data into hexadecimal.
		$unpack_binary_response = array_shift(unpack("H*", $binary_response));

		// build an array to pass data through into controller.
		$result_array = array(
			'length' 	=> $response_length-2,
			'cmd'		=> $cmd,
			'status' 	=> $status,
			'address' 	=> $addr,
			'title' 	=> $title,
		);

		// if unpack_binary_response not empty, add item into returning array. 
		if (!empty($unpack_binary_response)) {
	    	$result_array['hex_res'] = $unpack_binary_response;
		}

		// passing this array if socket succeed.
		return $result_array;
	}

	// Generate License VCG
	public function generate_vcg($vendorname, $pcmac)
	{
		// check if vendor name is 16 long and return valid ones.
		$vendorname = $this->check_input_length($vendorname, 16);

		// prefix(CMD) of data string to be passed.
		$prefix 	= "VC";
		
		// pack Number of Mac Address as Hexadecimal value.
		$num_mac 	= pack("H*", '01');

		// do the same to Mac Address.
		$macAddr 	= pack("H*", $pcmac);

		// length of request to be passed.
		$request_length = strlen($prefix) + strlen($vendorname) + strlen($num_mac) + strlen($macAddr);

		// convert length into hexadecimal.
		$request_length_hex = sprintf("%04x", $request_length);

		// then convert this hexadecimal value into binary.
		$request_length_binary = pack("H*", $request_length_hex);

		// request to be passed.
		$request = $request_length_binary.$prefix.$vendorname.$num_mac.$macAddr;
		
		// send request again with response length.
		$response = $this->socket_request_response($request);

		// if response return -1, socket error, return error status.
		if ($response == "-1") {
			return $result_array = array(
				'cmd' 		=> $prefix,
				'status' 	=> 'xxxx',
			);
		}

		// cut off the first two chars that represents length of response.
		$response_length 	= $response['response_length'];

		// content(length): cmd(2)+status(2)+path(43)+space(1)+title(11)+semicolon(1)+length(2)+data(256).
		$response_data		= $response['response_data'];

		$this->write_log_for_response_data($response_data);

		// then get the string without the last 256 chars.
		$header = substr($response_data, 0, -256);

		// the first two chars represent CMD passed through.
		$cmd 	= substr($header, 0, 2);

		// then get status code.
		$status = array_shift(unpack("H*", substr($response_data, 2, 2)));

		// then get address.
		$addr 	= substr($header, 5, -15);

		// trim "License Data" from the header string.
		$title 	= substr($header, -14, -3);

		// the last 256 chars are binary data.
		$binary_response = substr($response_data, -256);

		// convert this binary data into hexadecimal.
		$unpack_binary_response = array_shift(unpack("H*", $binary_response));

		// build an array to pass data through into controller.
		$result_array = array(
			'length' 	=> $response_length-2,
			'cmd'		=> $cmd,
			'status' 	=> $status,
			'address' 	=> $addr,
			'title' 	=> $title,
		);

		// if unpack_binary_response not empty, add item into returning array. 
		if (!empty($unpack_binary_response)) {
	    	$result_array['hex_res'] = $unpack_binary_response;
		}

		// passing this array if socket succeed.
		return $result_array;
	}

	// VCNumber Generation
	public function generate_vcnumber($vendorname, $dataprefix, $startcardnumber, $cardamount, $filename)
	{
		// check if vendor name is 16 long and return valid ones.
		$vendorname = $this->check_input_length($vendorname, 16);

		// prefix(CMD) of data string to be passed.
		$prefix 		= "CN";

		// pad zero before startcardnumber and cardamount if length of them are not 8.
		$startcardnumber = $this->pad_zero_before_numbers($startcardnumber);
		$cardamount 	= $this->pad_zero_before_numbers($cardamount);

		// pack checkflag as Hexadecimal value.
		$checkflag 		= pack("H*", '00');

		// do the same to startcardnumber and cardamount.
		$startcardnumber = pack("H*", $startcardnumber);
		$cardamount 	= pack("H*", $cardamount);

		// filename to be passed.
		// $filename 		= "TestingCardNumber.txt";

		// length of request to be passed.
		$request_length = strlen($prefix) + strlen($vendorname) + strlen($startcardnumber) + strlen($cardamount) + strlen($checkflag) + strlen($filename);

		// convert length into hexadecimal.
		$request_length_hex = sprintf("%04x", $request_length);

		// then convert this hexadecimal value into binary.
		$request_length_binary = pack("H*", $request_length_hex);

		// request to be passed.
		$request = $request_length_binary.$prefix.$vendorname.$startcardnumber.$cardamount.$checkflag.$filename;

		// send request again with response length.
		$response = $this->socket_request_response($request);

		// if response return -1, socket error, return error status.
		if ($response == "-1") {
			return $result_array = array(
				'cmd' 		=> $prefix,
				'status' 	=> 'xxxx',
			);
		}

		// cut off the first two chars that represents length of response.
		$response_length 	= $response['response_length'];

		$response_data		= $response['response_data'];

		$this->write_log_for_response_data($response_data);

		// the first two chars represent CMD passed through.
		$cmd 	= substr($response_data, 0, 2);

		// then get status code.
		$status = array_shift(unpack("H*", substr($response_data, 2, 2)));

		// then get number record.
		$number_record 	= array_shift(unpack("H*", substr($response_data, 4, 4)));

		// then get file name with path.
		$file_path		= substr($response_data, 8);

		// build an array to pass data through into controller.
		$result_array = array(
			'length' 		=> $response_length-2,
			'cmd'			=> $cmd,
			'status' 		=> $status,
		);

		// if number_record not empty, add item into returning array.
		if (!empty($number_record)) {
	    	$result_array['number_record'] = $number_record;
		}

		// if file_path not empty, add item into returning array.
		if (!empty($file_path)) {
	    	$result_array['file_path'] = $file_path;
		}

		// passing this array if socket succeed.
		return $result_array;
	}

	// Generate DKC
	public function generate_dkc($vendorname, $devicetype, $port)
	{
		// check if vendor name is 16 long and return valid ones.
		$vendorname = $this->check_input_length($vendorname, 16);

		// prefix and suffix of data string to be passed.
		$prefix 	= "DK";

		// pack port as Hexadecimal value.
		$port 			= pack("H*", '0'.$port);

		// length of request to be passed.
		$request_length = strlen($prefix) + strlen($vendorname) + strlen($devicetype) + strlen($port);

		// convert length into hexadecimal.
		$request_length_hex = sprintf("%04x", $request_length);

		// then convert this hexadecimal value into binary.
		$request_length_binary = pack("H*", $request_length_hex);

		// request to be passed.
		$request = $request_length_binary.$prefix.$vendorname.$devicetype.$port;

		// send request again with response length.
		$response = $this->socket_request_response($request);

		// if response return -1, socket error, return error status.
		if ($response == "-1") {
			return $result_array = array(
				'cmd' 		=> $prefix,
				'status' 	=> 'xxxx',
			);
		}

		// cut off the first two chars that represents length of response.
		$response_length 	= $response['response_length'];

		$response_data		= $response['response_data'];

		$this->write_log_for_response_data($response_data);

		// the first two chars represent CMD passed through.
		$cmd 	= substr($response_data, 0, 2);

		// then get status code.
		$status = array_shift(unpack("H*", substr($response_data, 2, 2)));

		// then get Card Sequence Number.
		$snumber 	= array_shift(unpack("H*", substr($response_data, 4)));

		// build an array to pass data through into controller.
		$result_array = array(
			'length' 		=> $response_length-2,
			'cmd'			=> $cmd,
			'devicetype'	=> $devicetype,
			'status' 		=> $status,
		);

		// if snumber not empty, add item into returning array.
		if (!empty($snumber)) {
	    	$result_array['snumber'] = $snumber;
		}

		// passing this array if socket succeed.
		return $result_array;
	}

	// write log for response data.
	private function write_log_for_response_data($response_data)
	{
		// open file.
		$file = './/application//logs//responsedata.log';
		if (!file_exists($file)) {
			$handle = fopen($file, "w");
		} else {
			$handle = fopen($file, "r");
		}

		// get existing content from the file.
		$current = file_get_contents($file);

		// Appen current time to content of the file.
		$current .= $response_data;
		// $current .= "\n"; //extra hex:0a

		// Write the contents back to the file
		file_put_contents($file, $current);

		// close file.
		fclose($handle);
	}

	// check input length.
	private function check_input_length($input, $length)
	{
		// check if length of $input is $length.
		if (strlen($input) <> $length) {
			// if length is larger than $length, trim only the first $length characters.
			if (strlen($input) > $length) {
				return substr($input, 0, $length);
			}
			// if length is less than $length, append enough empty whitespace.
			if (strlen($input) < $length) {
				return str_pad($input, $length);
			} 
		}

		// return input while if statement didn't execute.
		return $input;
	}

	// pad 0 before input for DPVC.
	private function pad_zero_before_numbers($input)
	{
		if (strlen($input) < 8) {
			$input = str_pad($input, 8, "0", STR_PAD_LEFT);
		}
		return $input;
	}

	// get response length.
	private function get_response_length_decimal($len_binary)
	{
		// unpack length of response.
		$unpackResponse = array_shift(unpack("H*", $len_binary));

		// convert this hexadecimal value into decimal.
		$length = hexdec($unpackResponse);

		return $length;
	}

	// Socket Connection function.
	private function socket_request_response($request)
	{
		// get CI instance to consume its native resouces.
		$CI =& get_instance();

		// try establish socket connection and catch exceptions.
		try {
			// create socket.
			$socket 	= socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
			// get socket connection params from config file.
			$params = $CI->config->item('socket_connection');
			// address and port to be connected.
			$address 	= $params['ip_address'];
			$port 		= $params['port'];
			// establish connection.
			$connection = socket_connect($socket, $address, $port);
			// error handling when connection fails.
			if ($connection === false) {
				// echo "socket_connect() failed.\nReason: ($connection) " . socket_strerror(socket_last_error($socket)) . "\n";
				return "-1";
			} 
			
			// send request to socket.
			socket_write($socket, $request, strlen($request)) or die("Could not send data to server\n");
			
			// read length of response from socket.
			$response_len_binary = $this->readSocketForDataLength($socket, 2);
			
			// convert response length from binary to decimal.
			$response_length = $this->get_response_length_decimal($response_len_binary);

			// get response with response length.
			$response_data = $this->readSocketForDataLength($socket, $response_length);

			// return -1 and error if response failed.
			if ($response_data === false) {
				echo "readSocketForDataLength failed.\nReason: ($connection) " . socket_strerror(socket_last_error($socket)) . "\n";
				return "-1";
			}

			// build array to contain both length and data.
			$response = array(
				'response_length' 	=> $response_length,
				'response_data'		=> $response_data,
			);

			// pass response array back.
			return $response;
		} catch (Exception $e) {
			var_dump($e);
		}
	}

	// read socket data.
	// see http://php.net/manual/en/function.socket-read.php for details.
	private function readSocketForDataLength ($socket, $len)
	{
	    $offset = 0;
	    $socketData = '';
	    
	    while ($offset < $len) {
	        if (($data = @socket_read ($socket, $len-$offset, $this->readType)) === false) {
	            $this->error();
	            return false;
	        }
	        
	        $dataLen = strlen ($data);
	        $offset += $dataLen;
	        $socketData .= $data;
	        
	        if ($dataLen == 0) { break; }
	    }

	    return $socketData;
	}
}

 ?>