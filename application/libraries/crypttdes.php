<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
*  Key Generator
* @author Felix Wang <felixwang@techtrex.com>
*/
include_once('crypt/TripleDES.php');

class CryptTdes extends Crypt_TripleDES
{
	public function __construct() {
		parent::__construct(CRYPT_MODE_CBC);
		$this->setKey('mbaf1nbdaqnz24ui');
		//$this->setIV('123456');
	}

}

 ?>