<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
*  Key Generator
* @author Felix Wang <felixwang@techtrex.com>
*/
class KeyGenerator
{
	/**
	 * @var \CI_Controller
	 */
	private $ci;
	private $openssl_config_path;
	private $ca_pub_key;
	private $ca_pra_key;

	public function __construct() {
		$this->ci = &get_instance();
		$this->openssl_config_path = $this->ci->config->config['openssl_config_path'];
		$this->ca_pub_key = $this->ci->config->config['ca_pub_key'];
		$this->ca_pra_key = $this->ci->config->config['ca_pra_key'];
	}

	public function generate_tdes_key(){
		$rand_decimal_coll = array();
		for($k = 0;$k < 14; $k++){
			$hex_group = '';
			for($i = 0;$i < 8; $i++){
				$hex_group .= sprintf("%02x",rand(1,255));
			}
			$rand_decimal_coll[] = $hex_group;
		}

		return implode('',$rand_decimal_coll);
	}

	/**
	 * @param array $required_info
	 * @return string
	 */
	public function combination($required_info = array()){
		$this->ci->load->library('crypttdes');

		$tdes_key = $this->generate_tdes_key();
		$rsa_server_key = $this->generate_rsa_server_key();
		$server_ca_certificate = $this->generate_server_ca_certificate($required_info['vendor_id']);


		$ca_pub_key_hex = $this->strToHex($this->ca_pub_key);
		$rsa_server_public_key_hex =  $this->strToHex($rsa_server_key['rsa_server_public_key']['key']);
		$rsa_server_private_key =  $this->strToHex($rsa_server_key['rsa_server_private_key']);

		$tag_1 = '01'.sprintf('%04x',strlen($tdes_key) >> 1).$tdes_key;
		$tag_2 = '02'.sprintf('%04x',strlen($ca_pub_key_hex) >> 1).$ca_pub_key_hex;
		$tag_3 = '03'.sprintf('%04x',strlen($rsa_server_public_key_hex) >> 1).$rsa_server_public_key_hex;
		$tag_4 = '04'.sprintf('%04x',strlen($rsa_server_private_key) >> 1).$rsa_server_private_key;
		$tag_5 = '05'.sprintf('%04x',strlen($server_ca_certificate) >> 1).$server_ca_certificate;

		$all_in_one_hex = $tag_1.$tag_2.$tag_3.$tag_4.$tag_5;
		$all_in_one = $this->hexToStr($all_in_one_hex);

		$encrypted_all_in_one = $this->ci->crypttdes->encrypt($all_in_one);

		$encrypted_all_in_one_hex = $this->strToHex($encrypted_all_in_one);
		$encrypted_all_in_one_hex_len = sprintf('%04x', strlen($encrypted_all_in_one_hex) >> 1);

		$this->write_log(array(
			'tag_1' => $tag_1,
			'tag_2' => $tag_2,
			'tag_3' => $tag_3,
			'tag_4' => $tag_4,
			'tag_5' => $tag_5,
			'combined_tags' => $all_in_one_hex,
			'encrypted_combined_tags' => base64_encode($encrypted_all_in_one),
			'encrypted_combined_tags_hex' => $encrypted_all_in_one_hex_len.$encrypted_all_in_one_hex
		));

		return $encrypted_all_in_one_hex_len.$encrypted_all_in_one_hex;
	}

	public function generate_rsa_server_key(){
		// for SSL server certificates the commonName is the domain name to be secured
		// for S/MIME email certificates the commonName is the owner of the email address
		// location and identification fields refer to the owner of domain or email subject to be secured
		$dn = array(
			"countryName" => "GB",
			"stateOrProvinceName" => "Somerset",
			"localityName" => "Glastonbury",
			"organizationName" => "The Brain Room Limited",
			"organizationalUnitName" => "PHP Documentation Team",
			"commonName" => "Felix Wang",
			"emailAddress" => "felixwang@techtrex.com"
		);

		// Generate a new private (and public) key pair
		$privkey = openssl_pkey_new(array(
			"private_key_bits" => 1024,
			"private_key_type" => OPENSSL_KEYTYPE_RSA,
			"config"       => $this->openssl_config_path,
		));

		// Generate a certificate signing request
		$csr = openssl_csr_new($dn, $privkey, array(
			'digest_alg' => 'sha256',
			"config"=> $this->openssl_config_path,
		));

		// Generate a self-signed cert, valid for 365 days
		$x509 = openssl_csr_sign($csr, null, $privkey, $days=365, array(
			'digest_alg' => 'sha256',
			"config"=> $this->openssl_config_path
		));

		// Save your private key, CSR and self-signed cert for later use
		//openssl_csr_export($csr, $csrout) and var_dump($csrout);
		//openssl_x509_export($x509, $certout) and var_dump($certout);
		//openssl_pkey_export($privkey, $pkeyout, NULL) and var_dump($pkeyout);
		$public_key=openssl_pkey_get_details($privkey); //and var_dump($public_key);
		openssl_pkey_export($privkey, $private_key,NULL, array('config'=>$this->openssl_config_path)); //and var_dump($private_key);
		// Show any errors that occurred here
		/*while (($e = openssl_error_string()) !== false) {
			echo $e . "\n";
		}*/

		return array(
			'rsa_server_public_key' => $public_key,
			'rsa_server_private_key' => $private_key,
		);
	}


	public function generate_server_ca_certificate($vendor_id){
		$vendor_id = str_pad($vendor_id,16,0,STR_PAD_LEFT);
		$vendor_id_hex_arr = array_map(function($v){
			return sprintf('%02x',$v);
		},self::convert_to_bcd(self::getBytes($vendor_id)));
		$vendor_id = implode('', $vendor_id_hex_arr);

		$ca_pub_modulus = $this->pub_key2modulus($this->ca_pub_key);

		$ca_pub_key_header_hex = substr($ca_pub_modulus['modulus'], 0,120);
		$ca_pub_key_footer_hex = substr($ca_pub_modulus['modulus'], 60);
		$ca_pub_key_footer = $this->hexToStr($ca_pub_key_footer_hex);

		$certificate_header         = $this->hexToStr('61');
		$version_number             = $this->hexToStr('01');
		$ca_key_version             = $this->hexToStr('01');
		$target_device_type         = $this->hexToStr('01');
		$target_serial_number       = $this->hexToStr($vendor_id);
		$target_key_version_number  = $this->hexToStr('01');
		$issue_date                 = $this->hexToStr(date('ymd'));
		$expiry_date                = $this->hexToStr(date('ymd'));
		$public_key_modulus         = $this->hexToStr($ca_pub_key_header_hex);
		$random                     = $this->dec2hex($this->generate_random_nums());

		$hash = $this->hexToStr(sha1( $certificate_header .
				$version_number .
				$ca_key_version .
				$target_device_type .
				$target_serial_number .
				$target_key_version_number .
				$issue_date.$expiry_date .
				$public_key_modulus .
				$random
			)
		);

		$signed_certificate = array('certificate_header' => $certificate_header,
			'version_number' => $version_number,
			'ca_key_version' => $ca_key_version,
			'target_device_type' => $target_device_type,
			'target_serial_number' => $target_serial_number,
			'target_key_version_number' => $target_key_version_number,
			'issue_date' => $issue_date,
			'expiry_date' => $expiry_date,
			'public_key_modulus' => $public_key_modulus,
			'random' => $random,
			'hash' => $hash,
			'ending' => $this->hexToStr('FE'),
		);

		$unencrypted_combined_fields = implode('', $signed_certificate);
		openssl_private_encrypt($unencrypted_combined_fields, $encrypted_body, $this->ca_pra_key);
		$server_ca_certificate_body = $ca_pub_key_footer.$encrypted_body;
		return $this->strToHex($server_ca_certificate_body);
	}

	public function pub_key2modulus($key){
		$res = openssl_pkey_get_public($key);
		$details = openssl_pkey_get_details($res);
		return array('modulus' => $this->strToHex($details['rsa']['n']) , 'publicExponent' => $this->strToHex($details['rsa']['e']));
	}

	public function hexToStr($hex){
		$str="";
		for($i=0;$i<strlen($hex)-1;$i+=2)
			$str.=chr(hexdec($hex[$i].$hex[$i+1]));
		return $str;
	}

	public function strToHex($str){
		$hex="";
		for($i=0;$i<strlen($str);$i++){
			$hex.=str_pad(dechex(ord($str[$i])),2,0,STR_PAD_LEFT);
		}
		$hex=strtoupper($hex);
		return $hex;
	}

	public function dec2hex($str){
		$hex="";
		for($i=0;$i<strlen($str);$i++) {
			$hex.= sprintf('%02x', $str[$i]);
		}
		return $this->hexToStr($hex);
	}

	// write key generator log
	private function write_log($result)
	{
		// open file.
		$file = './/application//logs//keygenerator.log';
		if (!file_exists($file)) {
			$handle = fopen($file, "w");
		} else {
			$handle = fopen($file, "r");
		}

		// get existing content from the file.
		$current = file_get_contents($file);

		// Appen current time to content of the file.
		$current .= "Log at [".date('Y-m-d H:i:s', time())."]:\n";

		// Append $result to content of the file.
		foreach ($result as $key => $value) {
			if('encrypted_combined_tags' != $key){
				// if array item is hex_array, then manipulate before writing into log file.
				$hex_array = str_split($value, 2);

				$hex_bundle = "";

				// add one white space between each two chars.
				foreach ($hex_array as $item) {
					$hex_bundle .= $item . " ";
				}

				// assign hex_bundle to value.
				$value = $hex_bundle;
			}

			// write repsonse data into log file.
			$current .= $key . ": " . $value ."\n";
		}
		$current .= "\n";

		// Write the contents back to the file
		file_put_contents($file, $current);

		// close file.
		fclose($handle);
	}

	public function generate_random_nums($length = 8){
		$seeds = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		$random_nums = '';
		for($i = 0; $i < $length; $i ++){
			$temp_val = $seeds[rand(0, sizeof($seeds) - 1)];
			$random_nums .= $temp_val;
		}
		return $random_nums;
	}

	public static function getBytes($string) {

		$bytes = array();
		for($i = 0; $i < strlen($string); $i++){
			$bytes[] = ord($string[$i]);
		}
		return $bytes;
	}

	public static function toStr($bytes) {
		$str = '';
		foreach($bytes as $ch) {
			$str .= bin2hex(chr($ch));
		}

		return $str;
	}

	public static function convert_to_bcd($bytes) {
		$temp_arr = array();
		for($i = 0; 2*$i <sizeof($bytes); $i++)
		{
			$temp_arr[$i] =  (chr($bytes[2*$i]) << 4);
			if(2*$i+1 >= sizeof($bytes)){
				break;
			}
			$temp_arr[$i] |= (chr($bytes[2*$i+1]) & 0x0f);
		}
		return $bcd_arr = $temp_arr;
	}

	public static function convert_to_dec($bcd) {
		$dec_arr = array();
		for($i = 0; $i < sizeof($bcd); $i++ )
		{
			$Temp = ($bcd[$i] & 0xf0) >> 4;
			if ($Temp < 10){
				$dec_arr[2*$i] =  (0x30 + $Temp);
			}

			$Temp = $bcd[$i] & 0x0f;
			if ($Temp < 10){
				$dec_arr[2*$i+1] = (0x30 + $Temp);
			}

		}
		return $dec_arr;
	}

}

 ?>