-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2013 at 03:09 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dms`
--
CREATE DATABASE IF NOT EXISTS `dms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `dms`;

-- --------------------------------------------------------

--
-- Table structure for table `card_number_generation`
--

CREATE TABLE IF NOT EXISTS `card_number_generation` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `date_generated` varchar(20) NOT NULL,
  `startcardnumber` int(20) NOT NULL,
  `cardamount` int(20) NOT NULL,
  `order_status` varchar(20) NOT NULL,
  `manufacturer` varchar(20) NOT NULL,
  `filename` varchar(60) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `license_generation`
--

CREATE TABLE IF NOT EXISTS `license_generation` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `vendorname` varchar(20) NOT NULL,
  `date_generated` varchar(20) NOT NULL,
  `filename` varchar(60) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  `action_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `vendorname` varchar(20) NOT NULL,
  `contactname` varchar(20) NOT NULL,
  `emailaddress` varchar(40) NOT NULL,
  `addressline1` varchar(40) NOT NULL,
  `addressline2` varchar(40) NOT NULL,
  `phonenumber` varchar(20) NOT NULL,
  `hex_res` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/* Manually insert vendors Phelps */
INSERT INTO vendor (`id`, `vendorname`, `contactname`, `emailaddress`, `addressline1`, `addressline2`, `phonenumber`, `hex_res`) VALUES (0, 'Phelps', '', '', '', '', '', 'de94836d771d6f27a1f154a77b82ae648b40ede09c5ba22d6cd1d3cd6363fe0cbd701b62ad5f6a8754f5c958abc90693a35d4f623b421f9986cad79850e610c518f7ba08895e275a2140a1020d775b06df04eb4e5824fe34068b7e42089138dc52b297cedac4bb7c64fd2313e81b1602d090614f13f6a9966bbb3bae0f3ac046bb96f041ad2ba742bedc3167899f534585f204f364e01b4f9ec2e0b0dc9654b1105bc92499a24f906408a34f8ae5722e6c1552883eb0f9b3b066956f163af565bf05b508e4f31a0dc9cfec3bb60b505f57fbe851e50ad8a397c4c1fdf13bd0b5f1efc764e65eef985b0bbc25e0a2ae76cd836825dae3f249ad80c583c4f86ef3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
