<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* User Model for Database Management
* @author Lyon Sun <lyonsun@techtrex.com>
*/
class User_Model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->dbutil();
		$db_exists = $this->dbutil->database_exists('dms');
		if ($db_exists) {
			$this->db->query('use dms');
		}
	}

	// validate if user is admin.
	public function validate_admin($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('users');

		if ($query->num_rows() > 0) {
			return true;
		}
		return false;
	}

	// get user information.
	public function get_user_info($username)
	{
		$this->db->where('username', $username);

		$query = $this->db->get('users');

		return $query;
	}

	// update user password.
	public function update_user_info($username, $password)
	{
		// data to be updated.
		$data = array(
			'password' 	=> $password,
		);
		
		// update users table.
		$this->db->where('username', $username);
		$query = $this->db->update('users', $data);

		// check if update succeed.
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		return false;
	}

	// insert vendor information into database.
	public function insert_vendor($data)
	{
		$this->db->where('vendorname', $data['vendorname']);
		$query 	= $this->db->get('vendor');

		// reason why execute the following: 
		// registry was deleted, but database remains no change. 
		// This is still not the best solution, it would be much better to check if registry exists, and then update this table, along with the directory under Release.
		if ($query->num_rows() > 0) {
			// if vendor name exists, update row.
			$this->db->where('vendorname', $data['vendorname']);
			$this->db->update('vendor', $data);
		} else {
			// otherwise, create new one.
			$this->db->insert('vendor', $data);
		}
	}

	// get vendor information from database.
	public function get_vendors()
	{
		$this->db->select('vendorname');

		$query = $this->db->get('vendor');

		$vendors = array();

		foreach ($query->result() as $row)
		{
		    $vendor = $row->vendorname;

		    array_push($vendors, $vendor);
		}

		return $vendors;
	}

	// insert card number generation info into MySQL database.
	public function insert_card_number_generation($data)
	{
		$this->db->insert('card_number_generation', $data); 
	}

	// get card number generation from database.
	public function get_card_number_generation()
	{
		$query = $this->db->get('card_number_generation');

		return $query;
	}

	// get card number generation info by id.
	public function get_card_number_generation_by_id($id)
	{
		$this->db->where('id', $id);

		$query = $this->db->get('card_number_generation');

		return $query;
	}

	// insert license generation info into MySQL database.
	public function insert_license_generation($data)
	{
		$this->db->where('filename', $data['filename']);
		$query 	= $this->db->get('license_generation');

		// reason why execute the following: 
		// registry was deleted, but database remains no change. 
		// This is still not the best solution, it would be much better to check if registry exists, and then update this table, along with the directory under Release.
		if ($query->num_rows() > 0) {
			// if vendor name exists, update row.
			$this->db->where('filename', $data['filename']);
			$this->db->update('license_generation', $data);
		} else {
			// otherwise, create new one.
			$this->db->insert('license_generation', $data);
		}
	}

	// get license generation info from MySQL database.
	public function get_license_generation()
	{
		$this->db->order_by("date_generated", "desc"); 
		$query = $this->db->get('license_generation');

		return $query;
	}

	// check if reg file is stored in database.
	public function is_reg_file_in_database($reg_file)
	{
		// var_dump(explode('.', $reg_file)[0]);
		$this->db->where('filename', explode('.', $reg_file)[0]);
		$query = $this->db->get('license_generation');

		if ($query->num_rows() > 0) {
			return true;
		}
		
		return false;
	}

	// execute sql file.
	public function execute_sql_file($file)
	{
		// get sql file contents.
		$sql_content = file_get_contents($file);

		// remove comments from sql file contents.
		$sql_no_comment = '';
        foreach (explode("\n", $sql_content) as $line){
            // cut whole content by line.
            if(isset($line[0]) && $line[0] != "#"){
                $sql_no_comment .= $line."\n";
            }            
        }
		
		// separate each sql query.
		$query_array = explode(";", $sql_no_comment);

		// import each sql query.
		foreach ($query_array as $query) {
			// trim white space for each query.
			$query = trim($query);
            if($query) {
            	// if query string is not empty, execute.
                $this->db->query($query);
            }
		}

		/* Manually insert vendors Phelps */
		$insertQuery = "INSERT INTO vendor (`id`, `vendorname`, `contactname`, `emailaddress`, `addressline1`, `addressline2`, `phonenumber`, `hex_res`) VALUES (0, 'Phelps', '', '', '', '', '', 'de94836d771d6f27a1f154a77b82ae648b40ede09c5ba22d6cd1d3cd6363fe0cbd701b62ad5f6a8754f5c958abc90693a35d4f623b421f9986cad79850e610c518f7ba08895e275a2140a1020d775b06df04eb4e5824fe34068b7e42089138dc52b297cedac4bb7c64fd2313e81b1602d090614f13f6a9966bbb3bae0f3ac046bb96f041ad2ba742bedc3167899f534585f204f364e01b4f9ec2e0b0dc9654b1105bc92499a24f906408a34f8ae5722e6c1552883eb0f9b3b066956f163af565bf05b508e4f31a0dc9cfec3bb60b505f57fbe851e50ad8a397c4c1fdf13bd0b5f1efc764e65eef985b0bbc25e0a2ae76cd836825dae3f249ad80c583c4f86ef3');";

		$this->db->where('vendorname', 'Phelps');
		$query = $this->db->get('vendor');

		// insert Phelps only if previous export doesn't include it.
		if (!$query->num_rows() > 0) {
			$this->db->query($insertQuery);
		} else {
			// data to be updated.
			$data = array(
				'id' 	=> 0,
			);
			
			// update vendor table.
			$this->db->where('vendorname', 'Phelps');
			$query = $this->db->update('vendor', $data);
		}
	}
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */