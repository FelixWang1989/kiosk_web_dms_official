<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Login Controller Class, manage user Login.
* @author Lyon Sun <lyonsun@techtrex.com>
*/
class Login extends CI_Controller {

	// Load common helper/library/models here.
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('user_model');
		$this->load->library('session');
		$this->load->dbutil();
	}

	// Index Page for this controller.
	public function index()
	{
		// check if database dms exists or not.
		$db_exists = $this->dbutil->database_exists('dms');

		// if not.
		if (!$db_exists) {
			// sql file to be execute.
			$file = "./application/core/dms_data/dms.sql";
			// execute sql file.
			$this->user_model->execute_sql_file($file);
			// set database configuration.
			// $db['default']['database'] = "dms";
		}

		// get data passing through POST.
		$postData = $this->input->post();

		$username = $postData['username'];
		$password = md5($postData['password']);

		if ($postData) {
			// if user bypass javascript checking.
			if (empty($username) || empty($postData['password'])) {
				$viewData['result'] = 'Username or Password cannot be empty.';
				$this->load->view('login', $viewData);
			} else {
				// if form is submitted, check if user is valid.
				$isAdmin = $this->user_model->validate_admin($username, $password);

				if ($isAdmin) {
					// if user data match db values, set userdata into session.
					$this->session->set_userdata('dms-user', $username);

					// then redirect user.
					$this->is_logged_in();
				} else {
					// otherwise, display login error page.
					$this->load->view('login_error');
				}
			}
		} else {
			// no form submission, check if session data is still valid.
			// redirect user if valid.
			$this->is_logged_in();
			// show login page if no session userdata.
			$this->load->view('login');
		}
	}

	// redirect user to index if session userdata is set.
    private function is_logged_in(){
        if ($this->session->userdata('dms-user')) {
            $login = base_url('user');
            redirect($login, 'location', 301);
        }
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */