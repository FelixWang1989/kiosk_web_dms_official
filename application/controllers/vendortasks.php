<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* VendorTasks Controller Class, manage all vendor tasks.
* @author Lyon Sun <lyonsun@techtrex.com>
*/
class VendorTasks extends CI_Controller {

    // Load common helper/library/models here.
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('datagenerator');
	      $this->load->library('keygenerator');
        $this->load->library('table');
        $this->load->model('user_model');
    }

    // Index Page for this controller.
    // list of all tasks for vendor.
    public function index()
    {
        // check if user is logged in.
        $this->checklogin();

        // get data passed throught GET.
        $getData = $this->input->get();

        $vendor_name = $getData['vendor'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // build array containing viewdata to pass through to ui.
        $viewData = array(
            'vendor_name' 	=> $vendor_name,
        );

        $this->load->view('vendor_tasks', $viewData);
    }

    // vendor task 1: LCMS License Generation.
    public function lcms()
    {
        // check if user is logged in.
        $this->checklogin();

        // get data passed throught GET.
        $getData 	= $this->input->get();
        $vendor_name = $getData['vendor'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // get data passed throught POST.
        $postData 	= $this->input->post();
        $pcmac 		= $postData['pcmac'];
        $pcmac 		= str_replace(':', '', $pcmac);
        $pcmac 		= strtoupper($pcmac);

        $snumber 	= $postData['snumber'];

        $vendor_id = $postData['vendor_id'];


        if ($postData) {
            // if there is submission, get result.
            $result = $this->datagenerator->generate_lcms($vendor_name, $pcmac, $snumber);

            // write result into log file.
            $this->write_log($result);

	          $licences = $this->keygenerator->combination(array('vendor_id' => $vendor_id));

            // set message to display if success.
            $succeed_msg = 'License generated successfully.';

            // get view data.
            $viewData = $this->get_view_data($result, $succeed_msg);

            if ($result['status'] == "0000") {
                // if license generated successfully, create license registry file.
                $file_path = $this->create_reg_for_license_generation($vendor_name, $result, 'LCMS', $pcmac, $snumber, NULL, array('licences' => $licences));
            }
        } else {
            // otherwise, return empty string.
            $viewData = array(
                'status'	=> '',
                'result' 	=> '',
            );
        }

        // array to hold all lcms reg files.
        $lcms_reg_files = $this->get_all_licenses($vendor_name, 'LCMS');

        // array to hold all license generated times.
        $generated_times = $this->get_license_generated_times();

        $viewData['generated_times'] = $generated_times;

        $viewData['deployed_licenses'] = $lcms_reg_files;

        $viewData['vendor_name'] 	= $vendor_name;

        $this->load->view('vendor_task_lcms', $viewData);
    }

    // vendor task 2: Value Code License Generation.
    public function valuecode()
    {
        // check if user is logged in.
        $this->checklogin();

        // get data passed throught GET.
        $getData 	= $this->input->get();
        $vendor_name = $getData['vendor'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // get data passed throught POST.
        $postData 	= $this->input->post();
        $pcmac 		= $postData['pcmac'];
        $pcmac 		= str_replace(':', '', $pcmac);
        $pcmac 		= strtoupper($pcmac);
        $port 		= (int)$postData['port'];


        if ($postData) {
            // if there is submission, get result.
            $result = $this->datagenerator->generate_vcg($vendor_name, $pcmac);

            // write result into log file.
            $this->write_log($result);

            if ($result['status'] == "0000") {
                // if license generated successfully, create license registry file.
                $file_path = $this->create_reg_for_license_generation($vendor_name, $result, 'VCG', $pcmac, null, $port);
            }

            // set message to display if success.
            $succeed_msg = 'License generated successfully.';

            // get view data.
            $viewData = $this->get_view_data($result, $succeed_msg);
        } else {
            // otherwise, return empty string.
            $viewData = array(
                'status'	=> '',
                'result' 	=> '',
            );
        }

        // array to hold all vcg reg files.
        $vcg_reg_files = $this->get_all_licenses($vendor_name, 'VCG');


        foreach ($vcg_reg_files as $v) {
            $f_sort[] = $v['f_time'];
        }

        array_multisort($f_sort, SORT_DESC, $vcg_reg_files);

        // array to hold all license generated times.
        $generated_times = $this->get_license_generated_times();

        $viewData['generated_times'] = $generated_times;

        $viewData['deployed_licenses'] = $vcg_reg_files;

        $viewData['vendor_name'] 	= $vendor_name;

        $this->load->view('vendor_task_valuecode', $viewData);
    }

    // vendor task 3: Data Preparation – Value Card.
    public function dpvc()
    {
        // check if user is logged in.
        $this->checklogin();

        // get data passed throught GET.
        $getData 	= $this->input->get();
        $vendor_name = $getData['vendor'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // get data passed throught POST.
        $postData 	= $this->input->post();
        $dataprefix 		= $postData['dataprefix'];
        $startcardnumber 	= $postData['startcardnumber'];
        $cardamount 		= $postData['cardamount'];

        if ($postData) {
            // set file name for this generation.
            $filename = $dataprefix.date("mdy").(int)$cardamount;

            $full_filename = $filename.'.txt';

            // if there is submission, get result.
            $result = $this->datagenerator->generate_vcnumber($vendor_name, $dataprefix, $startcardnumber, $cardamount, $full_filename);

            // write result into log file.
            $this->write_log($result);

            // set message to display if success.
            $succeed_msg = 'Value Card generated successfully.';

            // get view data.
            $viewData = $this->get_view_data($result, $succeed_msg);

            $order_status = $result['status'] == '0000' ? 'Generated' : 'Failed';

            // download link or not?
            $file_path = $result['status'] == '0000' ? $result['file_path'] : '';

            // save generation result into database, no matter succeed or failed.
            $generation_data 	= array(
                'date_generated' 	=> time(),
                'startcardnumber'	=> $startcardnumber,
                'cardamount'		=> $cardamount,
                'order_status'		=> $order_status,
                'manufacturer' 		=> strtoupper($vendor_name),
                'filename' 			=> $filename,
                'file_path'			=> $file_path,
            );
            $this->user_model->insert_card_number_generation($generation_data);

        } else {
            // otherwise, return empty string.
            $viewData = array(
                'status'	=> '',
                'result' 	=> '',
            );
        }

        $viewData['vendor_name'] 	= $vendor_name;

        $this->load->view('vendor_task_dpvc', $viewData);
    }

    // vendor task 4: Deployment Key Card Generation.
    public function dkcg()
    {
        // check if user is logged in.
        $this->checklogin();

        // get data passed throught GET.
        $getData 		= $this->input->get();
        $vendor_name 	= $getData['vendor'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // get data passed throught POST.
        $postData 		= $this->input->post();
        $devicetype 	= $postData['devicetype'];
        $port 			= $postData['port'];

        if ($postData) {
            // if there is submission, get result.
            $result = $this->datagenerator->generate_dkc($vendor_name, $devicetype, $port);

            // write result into log file.
            $this->write_log($result);

            // set message to display if success.
            $succeed_msg = 'Key Card generated successfully.';

            // get view data.
            $viewData = $this->get_view_data($result, $succeed_msg);
        } else {
            // otherwise, return empty string.
            $viewData = array(
                'status'	=> '',
                'result' 	=> '',
            );
        }

        $viewData['vendor_name'] 	= $vendor_name;

        $this->load->view('vendor_task_dkcg', $viewData);
    }

    // vendor task 5: View generated card numbers.
    public function viewgcn()
    {
        // check if user is logged in.
        $this->checklogin();

        $getData = $this->input->get();
        $vendor_name = $getData['vendor'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // add class for CI HTML table.
        $templete = array ('table_open'  => '<table class="table table-bordered">');
        $this->table->set_template($templete);

        // set heading for CI HTML table.
        $this->table->set_heading('Date Generated', 'Card Range (# of cards)', 'Order Status', 'Manufacturer', 'Download Link');

        // get all card number generations from database.
        $query = $this->user_model->get_card_number_generation();

        // if database is not empty.
        if ($query->num_rows > 0) {
            // put each record into CI HTML table.
            foreach ($query->result() as $row)
            {
                // date generated.
                $date_generated = $row->date_generated;
                // start card number.
                $startcardnumber = $row->startcardnumber;
                // card amount.
                $cardamount 	= $row->cardamount;
                // order status.
                $order_status 	= $row->order_status;
                // manufacturer.
                $manufacturer 	= $row->manufacturer;
                // end card number.
                $endcardnumber = $startcardnumber+$cardamount-1;
                // download link.
                $download_link = !empty($row->file_path) ? '<a href="download?vendor='.$vendor_name.'&id='.$row->id.'" title="Download Card Data">Download</a>' : '';

                // add rows into table.
                $this->table->add_row(date("d/m/Y", $date_generated), $startcardnumber. "-".$endcardnumber."(".$cardamount.")", $order_status, $manufacturer, $download_link);
            }

            // generate table.
            $table = $this->table->generate();

            $viewData['table'] = $table;
        }

        $viewData['vendor_name'] = $vendor_name;

        $this->load->view('vendor_task_viewgcn', $viewData);
    }

    // vendor task 6: Download generated card numbers.
    public function download()
    {
        // load zip library.
        $this->load->library('zip');

        // check if user is logged in.
        $this->checklogin();

        $getData = $this->input->get();
        $vendor_name 	= $getData['vendor'];
        $id 			= $getData['id'];

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // get card number matched by id passing from viewgcn.
        $query = $this->user_model->get_card_number_generation_by_id($id);

        // pulling nested array out.
        $row = array_shift($query->result());

        // file path of data to be zipped.
        $file_path = $row->file_path;

        // temporarily set memory limit to be unlimited, in case there comes large file to be read.
        ini_set('memory_limit', '-1');

        // read file for zipping.
        $this->zip->read_file($file_path);

        // name given for zipped file.
        $zipped_name = $row->filename . '.zip';

        // Popup download/where to save window.
        $this->zip->download($zipped_name);
    }

    // vendor task 7: Download generated license.
    public function download_license()
    {
        // load download helper.
        $this->load->helper('download');

        // check if user is logged in.
        $this->checklogin();

        $getData = $this->input->get();
        $vendor_name 	= $getData['vendor'];
        $filename 		= $getData['filename'].'.reg';
        $port   		= !empty($getData['port']) ? $getData['port'].'/' : '';

        // show error page if direct access.
        $this->no_direct_access($vendor_name);

        // directory to be scan to get all reg files.
        $dir_path 	= './Export/Release/'.ucfirst($vendor_name).'/'.$port;

        // data to be download.
        $data = file_get_contents($dir_path.$filename);

        // ci download.
        force_download($filename, $data);
    }

    // no direct access.
    private function no_direct_access($vendor_name=null)
    {
        // if no GET parameter, show 404 error page.
        if (!isset($vendor_name)) {
            show_404('page' , ['log_error']);
        }

        // retrieve vendor list from MySQL database.
        $vendors = $this->user_model->get_vendors();

        // if vendor is not in database, show 404 error page.
        if (!in_array($vendor_name, $vendors)) {
            show_404('page' , ['log_error']);
        }
    }

    // get all licenses generated.
    private function get_all_licenses($vendor_name, $action, $path = null)
    {
        // array to hold all lcms reg files.
        $files = array();

        // directory to be scan to get all reg files.
        $dir_path 	= './Export/Release/'.ucfirst($vendor_name).'/';

        // in case accidentally delete folder.
        if (!file_exists($dir_path)) {
            mkdir($dir_path, 0777, true);
        }

        // recursive path
        $path = !empty($path) ? $path : $dir_path;

        // is file
        if($result = glob($path.'*', GLOB_MARK|GLOB_BRACE)) {
            $result = str_replace('\\', '/', $result);
            foreach($result as $file) {
                if(substr($file, -1, 1) != '/'){
                    $file_path = dirname($file); // ./Export/Release/Phelps/22
                    $file = basename($file, '.reg');

                    // if exsting license database
                    $isInDB = $this->user_model->is_reg_file_in_database($file);
                    $vendor_actions = explode('_', $file);
                    if ($vendor_actions[0] == $action && $vendor_actions[1] == ucfirst($vendor_name) && $isInDB) {
                        // put port to list
                        list($_, $port) = explode($dir_path, $file_path);
                        $f_time = filectime($file_path.'/'.$file.'.reg');
                        $files[] = array('file' => $file, 'path' => $file_path, 'port' => str_replace('/', '', $port), 'f_time' => (int)$f_time);
                    }
                }
            }
        }

        // is dir
        if($result = glob($path.'*', GLOB_MARK|GLOB_ONLYDIR)) {
            $result = str_replace('\\', '/', $result);
            if (!empty($path) && !file_exists($result)) {
                mkdir($result, 0777, true);
            }
            foreach($result as $path) {
                $files = array_merge($files, $this->get_all_licenses($vendor_name, $action, $path));
            }
        }

        return $files;
    }

    // get license genereated times.
    private function get_license_generated_times()
    {
        // fetch every in table license_generation.
        $query = $this->user_model->get_license_generation();

        $generated_times = array();

        // push each item into array according to:
        // key: 	filename.reg
        // value: 	date_generated.
        foreach ($query->result() as $row) {
            $generated_times[$row->filename] = $row->date_generated;
        }

        return $generated_times;
    }

    // get view data accordingly.
    private function get_view_data($result, $succeed_msg)
    {
        // add status to array passing to ui view.
        $viewData = array(
            'status' 	=> 'Status: '.$result['status'],
        );

        $error_status = array(
            'xxxx' 	=> 'unable to make socket connection.',
            '0000' 	=> 'ERROR_SUCCESS',
            '0101' 	=> 'ERROR_REG_VALUE_NOT_FOUND',
            '0102' 	=> 'ERROR_REG_KEY_NOT_FOUND',
            '0103' 	=> 'ERROR_REG_VALUE_SET',
            '0104' 	=> 'ERROR_REG_KEY_CREATE',
            '0105' 	=> 'ERROR_REG_KEY_READ_OTHER',
            '0106' 	=> 'ERROR_CREAT_DKC_FAILED',
            '0001' 	=> 'ERROR_VENDOR_NOT_PRESENT',
            '0002' 	=> 'ERROR_VENDOR_EXISTS',
            '0003' 	=> 'ERROR_VENDOR_KEY_NOT_PRESENT',
            '0004' 	=> 'ERROR_PREVIOUS_PERSO_DATA_NOT_PRESENT',
            '0201' 	=> 'ERROR_INPUT_DATA_LENGTH',
            '0202' 	=> 'ERROR_INPUT_DATA_VALUE',
            '0203' 	=> 'ERROR_ACCOUNT_NUMBER_RANGE',
            '0204' 	=> 'ERROR_CARD_NUMBER_GENERATION',
        );

        // check if status return is 0000 or not.
        if ($result['status'] != "0000") {
            // if status is not 0000, error occurs.
            $error_msg = 'Something went wrong: ';

            switch ($result['status']) {
                case $result['status']:
                    $error_msg .= $error_status[$result['status']];
                    break;
                default:
                    $error_msg .= 'unpredictable';
                    break;
            }

            $viewData['result']	= $error_msg;
        } else {
            // otherwise, valid data returned, display successful result to ui.
            $viewData['result']	= $succeed_msg;
        }

        return $viewData;
    }

    // check login status.
    private function checklogin(){
        // redirect to login page if NO.
        if (!$this->session->userdata['dms-user']) {
            $login = base_url('login');
            redirect($login, 'location', 301);
        }
    }

    // write data generator log
    private function write_log($result)
    {
        // open file.
        $file = './/application//logs//datagenerator.log';
        if (!file_exists($file)) {
            $handle = fopen($file, "w");
        } else {
            $handle = fopen($file, "r");
        }

        // get existing content from the file.
        $current = file_get_contents($file);

        // Appen current time to content of the file.
        $current .= "Log at [".date('Y-m-d H:i:s', time())."]:\n";

        // Append $result to content of the file.
        foreach ($result as $key => $value) {
            if ($key == 'hex_res') {
                // if array item is hex_res, then manipulate before writing into log file.
                $hex_res_array = str_split($value, 2);

                $hex_res = "";

                // add one white space between each two chars.
                foreach ($hex_res_array as $item) {
                    $hex_res .= $item . " ";
                }

                // assign hex_res to value for key 'hex_res'.
                $value = $hex_res;
            }

            // write repsonse data into log file.
            $current .= $key . ": " . $value ."\n";
        }
        $current .= "\n";

        // Write the contents back to the file
        file_put_contents($file, $current);

        // close file.
        fclose($handle);
    }

    // create registry for license generation.
    private function create_reg_for_license_generation($vendor_name, $result, $action_name, $pcmac=null, $snumber=null, $port = null, $extra = array())
    {
        // get directory path.
        $dir_path = './Export/Release/'.ucfirst($vendor_name).'/'.$port.'/';

        // in case accidentally delete folder.
        if (!file_exists($dir_path)) {
            mkdir($dir_path, 0777, true);
        }

        // assign file name to be generated.
        $filename = $action_name.'_'.ucfirst($vendor_name).'_'.$port;

        if ($pcmac != null) {
            $filename .= '_'.$pcmac;
        }

        if ($snumber != null) {
            $filename .= '_'.$snumber;
        }

        // $filename .= '_'.date('H', time());

        // file to be created and edited.
        $file = $dir_path.$filename.'.reg';
        // open file.
        $handle = fopen($file, "w");

        // get existing content from the file, it will always be empty, cuz timestamp added when creating file.
        $current = file_get_contents($file);

        // Append title to content of the file.
        $current .= "Windows Registry Editor Version 5.00\r\n\r\n";

        // Append title to content of the file.
        $current .= "[".$result['address']."]\r\n";

        // manipulate hex_res before writing into log file.
        $hex_res_array = str_split($result['hex_res'], 2);

        // add one white space between each two chars.
        $hex_res = implode(",", $hex_res_array);

        // Append hex_res to content of the file.
        $current .= '"'.$result['title'].'"=hex:'.$hex_res."\r\n";

        // Append Mac Port
        $port = strlen(dechex($port))%2 == 0 ? dechex($port) : '0'.dechex($port);
        $port = strlen($port) == 2 ? '00'.$port : $port;
        $str_port = str_split($port, 2);
        $str_port = implode(",", $str_port);

        $current .= '"PolicyServerPort"=hex:'.$str_port."\r\n";

	      $licenses_hex_arr = str_split($extra['licences'],2);
	      $formatted_licenses_hex = implode(',',$licenses_hex_arr);
	      $current .= '"CertData"=hex:'.$formatted_licenses_hex."\r\n";

        $current .= "\r\n";

        // Write the contents back to the file
        file_put_contents($file, $current);

        // close file.
        fclose($handle);

        $licenseData = array(
            'vendorname' 		=> $vendor_name,
            'date_generated'  	=> time(),
            'filename' 			=> $filename,
            'file_path' 		=> $file,
            'action_name'		=> $action_name,
        );

        $this->user_model->insert_license_generation($licenseData);

        return $file;
    }
}

/* End of file vendortasks.php */
/* Location: ./application/controllers/vendortasks.php */