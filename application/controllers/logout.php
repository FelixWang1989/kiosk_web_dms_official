<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Logout Controller Class, manage user logout.
* @author Lyon Sun <lyonsun@techtrex.com>
*/
class Logout extends CI_Controller {
	
	// Load common helper/library/models here.
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
	}

	// Index Page for this controller.
	public function index()
	{
		// clear user session data and redirect to login page. 
		$this->session->unset_userdata('dms-user');
		$r = base_url('login');
		$this->output->set_header("Location: $r");
	}
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */