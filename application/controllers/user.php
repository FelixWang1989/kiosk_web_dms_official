<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* User Controller Class, manage all user tasks.
* @author Lyon Sun <lyonsun@techtrex.com>
*/
class User extends CI_Controller {

	// Load common helper/library/models here.
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('datagenerator');
		$this->load->model('user_model');
		$this->load->library('session');
		// specify database group beforehand.
		$this->db = $this->load->database('fordbutil', TRUE);
		// Load the DB utility class
		$this->load->dbutil();
	}
	
	// Index Page for this controller.
	// go here if login succeed.
	public function index()
	{
		// check if user is logged in.
		$this->checklogin();

		// get session userdata.
		$user = $this->session->userdata['dms-user'];

		// build array containing userdata to pass through to ui.
		$viewData = array(
			'user' 	=> $user,
		);

		// load view with data to be passed.
		$this->load->view('user_index', $viewData);
	}

	// user task 1: create new vendor.
	public function createnew()
	{
		// check if user is logged in.
		$this->checklogin();

		// get data passed throught POST.
		$postData = $this->input->post();

		// var_dump(isset($postData['mkey']));

		$vendor_name 	= $postData['vendorname'];
		$contactname 	= $postData['contactname'];
		$emailaddress	= $postData['emailaddress'];
		$addressline1	= $postData['addressline1'];
		$addressline2 	= $postData['addressline2'];
		$phonenumber 	= $postData['phonenumber'];

		// regex pattern.
		$pattern 		= '/^[a-zA-Z][a-zA-Z0-9]+$/i';

		if ($postData) {
			// if user bypass javascript
			if (empty($vendor_name)) {
				// vendor name empty.
				$viewData['result'] = 'Vendor Name cannot be empty.';
			} else if (strlen($vendor_name) > 16) {
				// vendor name has more than 16 chars.
				$viewData['result'] = 'Vendor Name cannot exceed 16 characters.';
			} else if (!preg_match($pattern, $vendor_name)) {
				// vendor name has special chars or start with number.
				$viewData['result'] = 'letters and numbers only, starting with number is not allowed.';
			} else {
				$mkey = isset($postData['mkey']) ? "1" : "0";
				// if there is submission, get result.
				$result = $this->datagenerator->create_vendor($vendor_name, $mkey);

				// write result into log file.
				$this->write_log($result);

				if ($result['status'] == "0000") {
					// if vendor created successfully, insert into database.
					$data = array(
						'vendorname' 	=> $vendor_name,
						'contactname'	=> $contactname,
						'emailaddress' 	=> $emailaddress,
						'addressline1'  => $addressline1,
						'addressline2' 	=> $addressline2,
						'phonenumber' 	=> $phonenumber,
						'hex_res' 		=> $result['hex_res'],
					);
					$this->user_model->insert_vendor($data);

					// also create a folder to store reg files later.
					$this->create_folder_for_vendor($vendor_name);
				}

				// get view data.
				$viewData = $this->get_view_data($result);
			}
		} else {
			// otherwise, return empty string.
			$viewData = array(
				'status'	=> '',
				'result' 	=> '',
			);
		}

		if (isset($result) && $result['status'] == '0000') {
			// load view after vendor creation success.
			$viewData['url'] 	= 'vendorlist';

			$this->load->view('user_redirect', $viewData);
			header('Refresh: 5; URL='.base_url('user/vendorlist').'');
		} else {
			// load view.
			$this->load->view('user_createnew', $viewData);			
		}
	}

	// user task 2: present a list of vendors.
	public function vendorlist()
	{
		// check if user is logged in.
		$this->checklogin();

		// load html table class from CI.
		$this->load->library('table');

		// set table class.
		$templete = array ('table_open'  => '<table class="table">');
		$this->table->set_template($templete);

		// retrieve vendor list from MySQL database.
		$vendors = $this->user_model->get_vendors();

		// generate each table row items.
		foreach ($vendors as $vendor)
		{
		    $this->table->add_row('<a href="'.base_url('vendortasks').'?vendor='.$vendor.'" title="'.$vendor.'">'.$vendor.'</a>');
		}

		// generate vendor list table.
		$table = $this->table->generate();

		$viewData = array(
			'table'			=> $table,
		);

		$this->load->view('user_vendorlist', $viewData);		
	}

	// user task 3&4: import/export data.
	public function transporter()
	{
		// check if user is logged in.
		$this->checklogin();

		// get data passed throught POST.
		$postData = $this->input->post();

		$viewData = array();
		// export
		if (isset($postData['exportfilename']) && !empty($postData['exportfilename'])) {
			// set export zip file name.
			$export_filename = $postData['exportfilename'];

			// export data: put reg/sql file and Techtrex folder into Export folder.
			$succeed = $this->export_data($export_filename);

			if (!$succeed) {
				$error_msg = "Sorry, there isn't any data to export. Make sure you have vendors created.";
			} else {
				$error_msg = 'Data exported successfully.';

				// if export succeed, prompt download window.
				$this->download_export_data($export_filename);
			}
			$viewData['result'] = $error_msg;
		} 
		// import
		else if (isset($postData['importdata']) && !empty($postData['importdata'])) {

			// upload zip file to server root directory.
			$config['upload_path'] = './';
			$config['allowed_types'] = 'zip';

			// load upload library.
			$this->load->library('upload', $config);

			// upload selected zip file.
            if (!$this->upload->do_upload('importfilename')) {
                $viewData['result'] = $this->upload->display_errors();
            } else {
            	// capture upload data.
                $uploadData = $this->upload->data();
            }

            // get file name from upload data.
            $import_filename = $uploadData['file_name'];
            
            // import data from uploaded zip file.
            $this->import_data($import_filename);

            $viewData['result'] = "Data imported successfully.";
		}


		$this->load->view('user_transporter', $viewData);
	}

	// user task 5: change password.
	public function changepassword()
	{
		// check if user is logged in.
		$this->checklogin();

		// get data passed throught POST.
		$postData = $this->input->post();

		$currentpass 	= $postData['currentpass'];
		$newpass	 	= $postData['newpass'];
		$repeatnew		= $postData['repeatnew'];

		if ($postData) {
			// get current user name from session.
			$user = $this->session->userdata['dms-user'];

			// get current user info from database.
			$query = $this->user_model->get_user_info($user);

			// user info array.
			$userinfo = array_shift($query->result_array());

			// check if currentpass from POST is the same as in db.
			if (md5($currentpass) != $userinfo['password']) {
				$error_msg 	= 'Current password you entered doesn\'t match that on our database.';
			} else {
				// otherwise continue.
				if (strlen($newpass) < 5) {
					// in case user bypass javascript, check if new password is 5 charcter long or not.
					$error_msg 	= 'Password must be at least 5 characters long.';
				} else if ($newpass != $repeatnew) {
					// in case user bypass javascript, check if new pass match or not again.
					$error_msg 	= 'New password given doesn\'t match.';
				} else {
					// otherwise continue.

					// update password, and inform user.
					$updated = $this->user_model->update_user_info($user, md5($newpass));
					
					if ($updated) {
						$error_msg 	= 'Password updated successfully.';						
					} else {
						$error_msg 	= 'Something unexpected went wrong.';
					}
				}
			}
			
			$viewData['result'] = $error_msg;
		} else {
			// no POST, no data.
			$viewData['result']	= '';
		}

		$this->load->view('user_changepassword', $viewData);
	}

	// user task 6: import original reg file.
	public function importregfile()
	{
		// check if user is logged in.
		$this->checklogin();

		$this->load->view('user_importregfile');	
	}

	// comfirm import original reg file.
	public function do_importregfile()
	{
		// check if user is logged in.
		$this->checklogin();

		exec("regedit /s ./application/core/dms_data/tls_dms.reg");

		$viewData['result'] = "Reg file imported successfully.";
		$viewData['url'] 	= 'index';

		$this->load->view('user_redirect', $viewData);
		header('Refresh: 5; URL='.base_url('user/index').'');
	}

	// export data to zip file.
	private function export_data($export_filename)
	{
		// directory containing all files.
		$dir_path = 'Export/';

		if (!file_exists($dir_path)) {
		    // mkdir($dir_path, 0777, true);
		    // $error_msg = "Sorry, there isn't any data to export.";
		    return false;
		}

		// export windows registry file into this dir.
		$cmd = "regedit /e ".$dir_path.$export_filename.".reg HKEY_CURRENT_USER\Software\TechTrexInc\TLS_DMS";

		exec($cmd);

		// parameters for database exporting.
		$prefs = array(
			'ignore'      => array(),           // List of tables to omit from the backup
			'format'      => 'txt',             // gzip, zip, txt
			'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
			'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
			'newline'     => "\n"               // Newline character used in backup file
		);

		// backup MySQL database.
		$backup = $this->dbutil->backup($prefs);

		// Load the file helper and write the file to your server.
		$this->load->helper('file');
		write_file($dir_path.'dms.sql', $backup);

		// source folder of card number generation.
		$srcDir = "C:/TechTrexInc/";

		// distination folder to copy source folder.
		$dstDir = "./Export/TechTrexInc/";

		// check if source folder exist before excute.
		if (file_exists($srcDir)) {
			$this->recurse_copy($srcDir, $dstDir);
		}


		return true;
	}

	// zip and download exported data.
	private function download_export_data($export_filename)
	{
		// load zip library.
		$this->load->library('zip');

		// directory containing all files.
		$dir_path = 'Export/';

		// temporarily set memory limit to be unlimited, in case there comes large file to be read.
		ini_set('memory_limit', '-1');
		
		// read file for zipping.
		$this->zip->read_dir($dir_path);

		// name given for zipped file.
		$zipped_file = $export_filename . '.zip';

		// Popup download/where to save window.
		$this->zip->download($zipped_file);

		$this->zip->clear_data();
	}

	// import data from zip file.
	private function import_data($import_filename)
	{
		// source file to be unzipped.
		$srcFile = './'.$import_filename;

		// unzip import file.
		$this->load->library('unzip');

		$this->unzip->extract($srcFile);

		// possible remove file after unzip?

		// source folder of card number generation.
		$srcDir = "./Export/TechTrexInc/";

		// distination folder to copy source folder.
		$dstDir = "C:/TechTrexInc/";

		// check if source folder exist before excute.
		if (file_exists($srcDir)) {
			// copy folder.
			$this->recurse_copy($srcDir, $dstDir);
			// delete folder.
			$this->delTree($srcDir);
		}

		// get all files in Export dir.
		$files = scandir('./Export');

		// get reg file and sql file.
		foreach ($files as $file) {
			$extension = explode('.', $file)[1];
			if ($extension == 'reg') {
				$reg_file = $file;
			} else if ($extension == 'sql') {
				$sql_file = $file;
			}
		}
		
		// import registry file silently.
		exec("regedit /s ./Export/".$reg_file);

		// execute sql file.
		$this->user_model->execute_sql_file("./Export/".$sql_file);
	}

	// delete folder.
	private function delTree($dir) { 
		$files = array_diff(scandir($dir), array('.','..')); 
		foreach ($files as $file) { 
		  (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file"); 
		} 
		return rmdir($dir); 
	} 

	// copy directory from one position to another.
	private function recurse_copy($src,$dst) { 
	    $dir = opendir($src); 
	    @mkdir($dst); 
	    while(false !== ( $file = readdir($dir)) ) { 
	        if (( $file != '.' ) && ( $file != '..' )) { 
	            if ( is_dir($src . '/' . $file) ) { 
	                $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	            else { 
	                copy($src . '/' . $file,$dst . '/' . $file); 
	            } 
	        } 
	    } 
	    closedir($dir); 
	} 

	// get view data accordingly.
	private function get_view_data($result)
	{
		// add status to array passing to ui view.
		$viewData = array(
			'status' 	=> 'Status: '.$result['status'],
		);

		$error_status = array(
			'xxxx' 	=> 'unable to make socket connection.',
			'0000' 	=> 'ERROR_SUCCESS',
			'0101' 	=> 'ERROR_REG_VALUE_NOT_FOUND',
			'0102' 	=> 'ERROR_REG_KEY_NOT_FOUND',
			'0103' 	=> 'ERROR_REG_VALUE_SET',
			'0104' 	=> 'ERROR_REG_KEY_CREATE',
			'0105' 	=> 'ERROR_REG_KEY_READ_OTHER',
			'0106' 	=> 'ERROR_CREAT_DKC_FAILED',
			'0001' 	=> 'ERROR_VENDOR_NOT_PRESENT',
			'0002' 	=> 'ERROR_VENDOR_EXISTS',
			'0003' 	=> 'ERROR_VENDOR_KEY_NOT_PRESENT',
			'0004' 	=> 'ERROR_PREVIOUS_PERSO_DATA_NOT_PRESENT',
			'0201' 	=> 'ERROR_INPUT_DATA_LENGTH',
			'0202' 	=> 'ERROR_INPUT_DATA_VALUE',
			'0203' 	=> 'ERROR_ACCOUNT_NUMBER_RANGE',
			'0204' 	=> 'ERROR_CARD_NUMBER_GENERATION',
		);

		// check if status return is 0000 or not.
		if ($result['status'] != "0000") {
			// if status is not 0000, error occurs.
			$error_msg = 'Something went wrong: ';
			
			switch ($result['status']) {
				case $result['status']:
					$error_msg .= $error_status[$result['status']];
					break;
				default:
					$error_msg .= 'unpredictable';
					break;
			}
			
			$viewData['result']	= $error_msg;
		} else {
			// otherwise, valid data returned, display successful result to ui.
			$succeed_msg = 'Vendor created successfully.';
			$viewData['result']	= $succeed_msg;
		}

		return $viewData;
	}

	// check login status.
    private function checklogin(){
        // redirect to login page if NO.
        if (!$this->session->userdata['dms-user']) {
			$login = base_url('login');
            redirect($login, 'location', 301);
        }
    }

    // write data generator log.
    private function write_log($result)
    {
    	// open file.
		$file = './/application//logs//datagenerator.log';
		if (!file_exists($file)) {
			$handle = fopen($file, "w");
		} else {
			$handle = fopen($file, "r");
		}

		// get existing content from the file.
		$current = file_get_contents($file);

		// Appen current time to content of the file.
		$current .= "Log at [".date('Y-m-d H:i:s', time())."]:\n";

		// Append $result to content of the file.
		foreach ($result as $key => $value) {
			if ($key == 'hex_res') {
				// if array item is hex_res, then manipulate before writing into log file.
				$hex_res_array = str_split($value, 2);

		    	$hex_res = "";

		    	// add one white space between each two chars.
		    	foreach ($hex_res_array as $item) {
		    		$hex_res .= $item . " ";
		    	}

		    	// assign hex_res to value for key 'hex_res'.
		    	$value = $hex_res;
			}

			// write repsonse data into log file.
		    $current .= $key . ": " . $value ."\n";
		}
		$current .= "\n";

		// Write the contents back to the file
		file_put_contents($file, $current);

		// close file.
		fclose($handle);
    }

    // create folder for vendor for further use.
    private function create_folder_for_vendor($vendor_name)
    {
    	$dir_path = './Export/Release/'.ucfirst($vendor_name);

    	if (!file_exists($dir_path)) {
		    mkdir($dir_path, 0777, true);
		}
    }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */