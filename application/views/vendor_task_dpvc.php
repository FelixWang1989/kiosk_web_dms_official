<?php $this->load->view('common/header.php'); ?>

			<h4>
				<?php echo strtoupper($vendor_name); ?> - <?php echo "DATA PREPARATION FOR VALUE CARD PRODUCTION"; ?>
			</h4>
			<span><a href="<?php echo base_url('vendortasks').'?vendor='.$vendor_name; ?>" class="pull-right" title="Back">Back</a></span><br>
			<?php if(!empty($result)) : ?>
			<div id="notification_msg" class="alert alert-info">
				<span><b><?php echo $result; ?></b></span>
			</div>
			<?php endif; ?>
			<div class="row vendor-task">
				<div class="col-xs-6 vendor-task-block">
					<h5>Generate Card Data</h5>
					<form class="form-horizontal" role="form" method="POST" action="<?php echo current_url().'?vendor='.$vendor_name; ?>">

						<div class="form-group">
							<label for="dataprefix" class="col-xs-4 control-label">Data Prefix: </label>
							<div class="col-xs-8">
								<input type="text" size="6" maxlength="6" class="form-control" name="dataprefix" value="" placeholder="CL0001">
							</div>
						</div>
						
						<div class="form-group">
							<label for="startcardnumber" class="col-xs-4 control-label">Start Card Number: </label>
							<div class="col-xs-8">
								<input type="text" size="8" maxlength="8" class="form-control" name="startcardnumber" value="" placeholder="Start Card Number">
							</div>
						</div>

						<div class="form-group">
							<label for="cardamount" class="col-xs-4 control-label">Number of cards: </label>
							<div class="col-xs-8">
								<input type="text" size="8" maxlength="8" class="form-control" name="cardamount" value="" placeholder="Number of cards">
							</div>
						</div>

						<div class="form-group">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-8">
						    	<input type="submit" class="btn btn-default btn-generate" value="Generate"></input>
							</div>
						</div>
					</form>
				</div>
				<div class="col-xs-6">
					<div class="row">
						<div class="col-xs-12 vendor-task-block">
							<h5>View generated card numbers: </h5>
							<a href="viewgcn?vendor=<?php echo $vendor_name; ?>" class="view-detail">Click here to view</a>
						</div>
					</div>
				</div>
			</div>
			
	    <script>
	    	$('input[type="submit"]').bind('click', function() {
	    		var dataprefix 		= $('input[name="dataprefix"]').val();
	    		var startcardnumber = $('input[name="startcardnumber"]').val();
	    		var cardamount 		= $('input[name="cardamount"]').val();

	    		if (dataprefix == '') {
	    			alert('Data Prefix cannot be empty.');
	    			$('input[name="dataprefix"]').focus();

	    			return false;
	    		};

	    		if (startcardnumber == '') {
	    			alert('Start Card Number cannot be empty.');
	    			$('input[name="startcardnumber"]').focus();

	    			return false;
	    		};

	    		if (cardamount == '') {
	    			alert('Number of cards cannot be empty.');
	    			$('input[name="cardamount"]').focus();

	    			return false;
	    		};

	    		return true;
	    	});
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>