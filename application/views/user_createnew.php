<?php $this->load->view('common/header.php'); ?>

			<h4>Create New Vendor</h4><span><a href="index" class="pull-right" title="Back">Back</a></span><br>
			<?php if(!empty($result)) : ?>
			<div id="notification_msg" class="alert alert-info">
				<span><b><?php echo $result; ?></b></span>
			</div>
			<?php endif; ?><br>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo current_url(); ?>">
				<div class="form-group">
					<label for="vendorname" class="col-xs-3 control-label">Vendor Name</label>
					<div class="col-xs-5">
						<input type="text" class="form-control" size="50" name="vendorname" value="" placeholder="Vendor Name" required>
					</div>
					<div class="col-xs-4 help-text">
						<span>letters and numbers only, starting with number is not allowed.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="contactname" class="col-xs-3 control-label">Contact Name</label>
					<div class="col-xs-5">
						<input type="text" class="form-control" size="50" name="contactname" value="" placeholder="Contact Name">
					</div>
					<div class="col-xs-4 help-text">
						<span>e.g. John Smith</span>
					</div>
				</div>
				<div class="form-group">
					<label for="emailaddress" class="col-xs-3 control-label">Email Address</label>
					<div class="col-xs-5">
						<input type="email" class="form-control" size="50" name="emailaddress" value="" placeholder="Email Address">
					</div>
					<div class="col-xs-4 help-text">
						<span>e.g. someone@example.com</span>
					</div>
				</div>
				<div class="form-group">
					<label for="addressline1" class="col-xs-3 control-label">Address Line 1</label>
					<div class="col-xs-5">
						<input type="text" class="form-control" size="50" name="addressline1" value="" placeholder="Address Line 1">
					</div>
					<div class="col-xs-4 help-text">
						
					</div>
				</div>
				<div class="form-group">
					<label for="addressline2" class="col-xs-3 control-label">Address Line 2</label>
					<div class="col-xs-5">
						<input type="text" class="form-control" size="50" name="addressline2" value="" placeholder="Address Line 2">
					</div>
					<div class="col-xs-4 help-text">
						
					</div>
				</div>
				<div class="form-group">
					<label for="phonenumber" class="col-xs-3 control-label">Phone Number</label>
					<div class="col-xs-5">
						<input type="text" class="form-control" size="10" maxlength="10" name="phonenumber" value="" placeholder="Phone Number">
					</div>
					<div class="col-xs-4 help-text">
						<span>e.g. '(000) 000-0000'</span>
					</div>
				</div>

				<div class="form-group">
					<div class="col-xs-3">
					</div>
					<div class="col-xs-5">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="mkey" value="mkey">
								M-Key
							</label>
						</div>
					</div>
					<div class="col-xs-4 help-text">
						<span>Keep  the same M-Key A, B (version 0 keys) and hash keys instead of new random key values.</span>
					</div>

				</div>
				
				<div class="form-group">
					<div class="col-xs-3">
					</div>
					<div class="col-xs-9">
				    	<input type="submit" class="btn btn-default btn-create" value="Create"></input>
				    	<a href="index" title="Cancel">Cancel</a>
					</div>
				</div>
			</form>
			
	    <script>
	    	$('input[type="submit"]').bind('click', function() {
	    		var vendorname 	= $('input[name="vendorname"]').val();
	    		var contactname = $('input[name="contactname"]').val();
	    		var emailaddress = $('input[name="emailaddress"]').val();
	    		var addressline1 = $('input[name="addressline1"]').val();
	    		var addressline2 = $('input[name="addressline2"]').val();
	    		var phonenumber = $('input[name="phonenumber"]').val();
	    		var _url 		= '<?php echo current_url(); ?>/ajax_submit';

	    		if (vendorname == '') {
	    			alert('Vendor Name cannot be empty.');
	    			$('input[name="vendorname"]').focus();

	    			return false;
	    		};

	    		if (vendorname.length > 16) {
	    			alert('Vendor Name cannot exceed 16 characters.');
	    			$('input[name="vendorname"]').focus();

	    			return false;
	    		};

	    		var reg_vendorname 	= /^[a-zA-Z][a-zA-Z0-9]+$/i;

	    		if (!vendorname.match(reg_vendorname)) {
	    			alert('letters and numbers only, starting with number is not allowed.');
	    			$('input[name="vendorname"]').focus();
	    			return false;
	    		};

	    		var reg_contactname = /^[a-zA-Z]+\s[a-zA-Z]+$/i;
	    		if (contactname != '') {
	    			if (!contactname.match(reg_contactname)) {
		    			alert('Please give valid contact name.');
		    			$('input[name="contactname"]').focus();
		    			return false;
		    		};
	    		};

/*
	    		if (contactname == '') {
	    			alert('Contact Name cannot be empty.');
	    			$('input[name="contactname"]').focus();

	    			return false;
	    		};

	    		if (emailaddress == '') {
	    			alert('Email Address cannot be empty.');
	    			$('input[name="emailaddress"]').focus();

	    			return false;
	    		};

	    		if (addressline1 == '') {
	    			alert('Address Line 1 cannot be empty.');
	    			$('input[name="addressline1"]').focus();

	    			return false;
	    		};

	    		if (addressline2 == '') {
	    			alert('Address Line 2 cannot be empty.');
	    			$('input[name="addressline2"]').focus();

	    			return false;
	    		};

	    		if (phonenumber == '') {
	    			alert('Phone Number cannot be empty.');
	    			$('input[name="phonenumber"]').focus();

	    			return false;
	    		};
*/
	    		return true;
	    	});
			$(document).ready(function() {
				$('input[name="phonenumber"]').iMask({
				      type      : 'fixed'
				    , mask      : '(999) 999-9999'
				    , stripMask : true
				});
			});
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>