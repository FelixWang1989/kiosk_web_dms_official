<?php $this->load->view('common/header.php'); ?>

			<h4>
				<?php echo strtoupper($vendor_name); ?> - <?php echo "DEPLOYMENT KEY CARD GENERATION"; ?>
			</h4>
			<span><a href="<?php echo base_url('vendortasks').'?vendor='.$vendor_name; ?>" class="pull-right" title="Back">Back</a></span><br>
			<?php if(!empty($result)) : ?>
			<div id="notification_msg" class="alert alert-info">
				<span><b><?php echo $result; ?></b></span>
			</div>
			<?php endif; ?>
			<div class="row vendor-task">
				<div class="col-xs-6 vendor-task-block">
					<h5>Generate Key Card</h5>
					<form class="form-horizontal" role="form" method="POST" action="<?php echo current_url().'?vendor='.$vendor_name; ?>">
						<div class="form-group">
							<label for="port" class="col-xs-4 control-label">Port: </label>
							<div class="col-xs-8">
								<input type="text" size="6" maxlength="6" class="form-control" name="port" value="" placeholder="1">
							</div>
						</div>
						
						<div class="form-group">
							<label for="devicetype" class="col-xs-4 control-label">Device Type: </label>
							<div class="col-xs-8">
								<select name="devicetype" class="form-control">
									<option value="PTS">PTS</option>
									<option value="LMC">LMC</option>
								</select>
							</div>
						</div>
						
						<div class="help-text">
							Place blank card on the reader and then click Generate button below.
						</div>

						<div class="form-group">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-8">
						    	<input type="submit" class="btn btn-default btn-generate" value="Generate"></input>
							</div>
						</div>
					</form>
				</div>
				<div class="col-xs-6">
					<div class="row">
						<div class="col-xs-12 vendor-task-block">
							<h5>View deployed PT-S: </h5>
							<a href="#" class="view-detail">Click here to view</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 vendor-task-block">
							<h5>View deployed LMC: </h5>
							<a href="#" class="view-detail">Click here to view</a>
						</div>
					</div>
				</div>
			</div>

	    <script>
	    	$('input[type="submit"]').bind('click', function() {
	    		var port 			= $('input[name="port"]').val();

	    		if (port == '') {
	    			alert('Port cannot be empty.');
	    			$('input[name="port"]').focus();

	    			return false;
	    		};

	    		return true;
	    	});
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>