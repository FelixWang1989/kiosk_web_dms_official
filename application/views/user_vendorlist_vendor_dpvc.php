<?php $this->load->view('common/header.php'); ?>

			<h4>
				<?php echo strtoupper($vendor_name); ?> - <?php echo "DATA PREPARATION FOR VALUE CARD PRODUCTION"; ?>
			</h4>
			<br>
			<div class="row">
				<div class="col-lg-6 vendor-task-block">
					<h5>Generate Card Data</h5>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="dataprefix" class="col-lg-4 control-label">Data Prefix: </label>
							<div class="col-lg-8">
								<input type="text" class="form-control" name="dataprefix" value="" placeholder="CL0001">
							</div>
						</div>
						
						<div class="form-group">
							<label for="startcardnumber" class="col-lg-4 control-label">Start Card Number: </label>
							<div class="col-lg-8">
								<input type="text" class="form-control" name="startcardnumber" value="" placeholder="Start Card Number">
							</div>
						</div>

						<div class="form-group">
							<label for="cardamount" class="col-lg-4 control-label">Number of cards: </label>
							<div class="col-lg-8">
								<input type="text" class="form-control" name="cardamount" value="" placeholder="Number of cards">
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-4 col-lg-8">
						    	<button type="submit" class="btn btn-default btn-generate">Generate</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12 vendor-task-block">
							<h5>View generated card numbers: </h5>
							<a href="viewgcn" class="view-detail">Click here to view</a>
						</div>
					</div>
				</div>
			</div>
			
	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>