<?php $this->load->view('common/header.php'); ?>

			<h4>Redirecting...</h4>
			<br><br><br>
			<?php if(!empty($result)) : ?>
			<div id="notification_msg" class="alert alert-info">
				<span><b><?php echo $result; ?></b></span>
				<p>Please wait...</p>
				<p>You will be redirected to <?php echo $url; ?> page in a few seconds.</p>
				<p>Click <a href="<?php echo $url; ?>" title="Here">Here</a> if you can't wait long.</p>
			</div>
			<?php endif; ?><br>
			
			<?php 
				// header('Refresh: 5; URL='.base_url('user/vendorlist').'');
			 ?>
			
	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>