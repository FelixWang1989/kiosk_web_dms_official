<?php $this->load->view('common/header.php'); ?>

			<h4>
				<?php echo strtoupper($vendor_name); ?> - <?php echo "DEPLOYMENT KEY CARD GENERATION"; ?>
			</h4>
			<br>
			<div class="row">
				<div class="col-lg-6 vendor-task-block">
					<h5>Generate Key Card</h5>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="devicetype" class="col-lg-4 control-label">Device Type: </label>
							<div class="col-lg-8">
								<!-- <input type="text" class="form-control" name="devicetype" value="" placeholder="PC MAC"> -->
								<select name="devicetype" class="form-control">
									<option value="typeone">Type One</option>
									<option value="typetwo">Type Two</option>
								</select>
							</div>
						</div>
						
						<div class="help-text">
							Place blank card on the reader and then click Generate button below.
						</div>

						<div class="form-group">
							<div class="col-lg-offset-4 col-lg-8">
						    	<button type="submit" class="btn btn-default btn-generate">Generate</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12 vendor-task-block">
							<h5>View deployed PT-S: </h5>
							<a href="#" class="view-detail">Click here to view</a>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 vendor-task-block">
							<h5>View deployed LMC: </h5>
							<a href="#" class="view-detail">Click here to view</a>
						</div>
					</div>
				</div>
			</div>

	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>