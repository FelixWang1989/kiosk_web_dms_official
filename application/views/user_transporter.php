<?php $this->load->view('common/header.php'); ?>

			<h4>Import/Export Data</h4>
			<span><a href="index" class="pull-right" title="Back">Back</a></span><br>
			<?php if(!empty($result)) : ?>
			<div id="notification_msg" class="alert alert-info">
				<span><b><?php echo $result; ?></b></span>
			</div>
			<?php endif; ?><br>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo current_url(); ?>">
				<div class="form-group">
					<label for="exportfilename" class="col-xs-3 control-label">Export File Name: </label>
					<div class="col-xs-6">
						<input type="text" class="form-control" name="exportfilename" value="" placeholder="">
						<span>.zip</span>
					</div>
					<div class="col-xs-3">
						<input type="submit" class="btn btn-default" name="exportdata" value="Export">
					</div>
				</div>
				<p class="help-block">Enter the file name and click on Export button to export the data. The file will be saved in a directory on the computer you choose.</p>
			</form>

			<form class="form-horizontal" role="form" method="POST" action="<?php echo current_url(); ?>" enctype="multipart/form-data">
				<div class="form-group">
					<label for="importfilename" class="col-xs-3 control-label">Import File Name: </label>
					<div class="col-xs-6">
						<input type="file" name="importfilename" class="form-control" value="Choose File..." placeholder="">
					</div>
					<div class="col-xs-3">
						<input type="submit" class="btn btn-default" name="importdata" value="Import">
					</div>
				</div>
				<p class="help-block">Select the file name from the drop down list and click on Import button to import the data.</p>
			</form>

	    <script>
	    	$('input[name="exportdata"]').bind('click', function() {
	    		var exportfilename = $('input[name="exportfilename"]').val();

	    		if (exportfilename == '') {
	    			alert("Please give a file name.");
	    			$('input[name="exportfilename"]').focus();

	    			return false;
	    		};

	    		return true;
	    	});
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>