<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Deployment Management System</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>static/css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>static/css/signin.css">
		<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>static/css/non-responsive.css"> -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>static/css/style.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>static/css/main.css">
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="<?php echo base_url(); ?>static/js/jquery-1.10.2.min.js"></script>
	</head>
	<body>
		<div class="mainWrapper">
			<div class="header clearfix">

				<a href="<?php echo base_url(); ?>" title="Deployment Management System - TechTrex Inc.">
				  
				      
				<img src="<?php echo base_url(); ?>static/css/images/dms-logo.jpg" width="257" height="90" alt="Deployment Management System">

				</a>

			</div>

			
			<div class="contentWrapper">
				<div class="content">				
				<header>
					<div class="titleHeader">
						<h1>Deployment Management System</h1>
					</div>
				</header><!-- /header -->
					<div class="container">