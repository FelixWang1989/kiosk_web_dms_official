<?php $this->load->view('common/header.php'); ?>
			<form class="form-horizontal" method="POST" action="<?php echo current_url(); ?>">
		        <h2 class="form-signin-heading">Please sign in</h2>
				<?php if(!empty($result)) : ?>
				<div id="notification_msg" class="alert alert-info">
					<span><b><?php echo $result; ?></b></span>
				</div>
				<?php endif; ?><br>
		        <div class="form-group">
    		        <label for="username" class="control-label col-xs-4">Enter Your Username</label>
    		        <div class="col-xs-4">
    		        	<input name="username" type="text" class="form-control" placeholder="User Name" autofocus>
    		        </div>
		        </div>
		        <div class="form-group">
		        	<label for="password" class="control-label col-xs-4">Password</label>
    		        <div class="col-xs-4">
    		        	<input name="password" type="password" class="form-control" placeholder="Password">
    		        </div>
    		    </div>
		        <!-- <label class="checkbox">
		          <input type="checkbox" value="remember-me"> Remember me
		        </label> -->
		        <div class="form-group">
		        	<div class="col-xs-4">
		        	</div>
		        	<div class="col-xs-4">
		        		<button class="btn btn-default btn-block btn-login" type="submit">Sign in</button>
		        	</div>
		        </div>
	      	</form>

	    <script>
	    	$('.btn-login').click(function () {
	    		var username = $('input[name="username"]').val();
	    		var password = $('input[name="password"]').val();

	    		if (username == "") {
	    			alert('Please enter your username.');
	    			$('input[name="username"]').focus();
	    			return false;
	    		};

	    		if (password == "") {
	    			alert('Please enter your password.');
	    			$('input[name="password"]').focus();
	    			return false;
	    		};

	    		return true;
	    	})
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>