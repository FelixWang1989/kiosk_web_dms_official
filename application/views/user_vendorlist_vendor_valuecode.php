<?php $this->load->view('common/header.php'); ?>

			<h4>
				<?php echo strtoupper($vendor_name); ?> - <?php echo "VALUE CODE LICENSE GENERATION"; ?>
			</h4>
			<br>
			<div class="row">
				<div class="col-lg-6 vendor-task-block">
					<h5>License Generation</h5>
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="pcmac" class="col-lg-4 control-label">PC MAC: </label>
							<div class="col-lg-8">
								<input type="text" class="form-control" name="pcmac" value="" placeholder="PC MAC">
							</div>
						</div>
						
						<div class="form-group">
							<label for="snumber" class="col-lg-4 control-label">S/N Deployed Package: </label>
							<div class="col-lg-8">
								<input type="text" class="form-control" name="snumber" value="" placeholder="S/N Deployed Package">
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-4 col-lg-8">
						    	<button type="submit" class="btn btn-default btn-generate">Generate</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12 vendor-task-block">
							<h5>View Licenses Deployed:</h5>
							<a href="#">&lt;Install Dir&gt;\Release\Phelps\VCG_Phelps_n.n.reg</a>
						</div>
					</div>
				</div>
			</div>

	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>