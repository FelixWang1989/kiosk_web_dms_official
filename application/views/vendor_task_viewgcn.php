<?php $this->load->view('common/header.php'); ?>

<?php 

/**
 * View: generated valuecard list page for logged in users.
 * @author Lyon Sun <lyonsun@techtrex.com>
 */

 ?>

			<h4>
				<?php echo strtoupper($vendor_name); ?> - <?php echo "VALUE CARD GENERATED NUMBERS"; ?>
			</h4>
			<span><a href="<?php echo base_url('vendortasks').'/dpvc?vendor='.$vendor_name; ?>" class="pull-right" title="Back">Back</a></span><br>
			<br>
			<?php 
				echo !isset($table) ? "<div class='notabledata'>No Data</div> " : $table;
			 ?>
			
	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>