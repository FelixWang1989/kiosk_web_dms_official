<?php $this->load->view('common/header.php'); ?>

<?php 

/**
 * View: Index/Home page for logged in users.
 * @author Lyon Sun <lyonsun@techtrex.com>
 */

 ?>

			<h4>Welcome <?php echo $user; ?>!</h4>

			<span class="pull-right"><a href="<?php echo base_url('logout'); ?>" title="Log Out">Log Out</a></span>
			
			<p>Please run the following option <b>if you are new here</b>: </p>
			
			<div class="row admintask">
				<div class="col-xs-2">
					
				</div>
				<div class="col-xs-4">
					<a href="<?php echo base_url('user/importregfile'); ?>" class="btn btn-default" title="Import Reg File">Import Reg File</a>
				</div>
				<div class="col-xs-6">
					
				</div>
			</div>

			<p>Otherwise, please select from an option below:</p>
			<div class="row admintask">
				<div class="col-xs-2">
					
				</div>
				<div class="col-xs-4">
					<a href="<?php echo base_url('user/createnew'); ?>" class="btn btn-default" title="Create New Vendor">Create New Vendor</a>
				</div>
				<div class="col-xs-6">
					<a href="<?php echo base_url('user/transporter'); ?>" class="btn btn-default" title="Import Data">Import Data</a>
				</div>
			</div>
			<div class="row admintask">
				<div class="col-xs-2">
					
				</div>
				<div class="col-xs-4">
					<a href="<?php echo base_url('user/vendorlist'); ?>" class="btn btn-default" title="Existing Vendors">Existing Vendors</a>
				</div>
				<div class="col-xs-6">
					<a href="<?php echo base_url('user/transporter'); ?>" class="btn btn-default" title="Export Data">Export Data</a>
				</div>
			</div>
			<div class="row admintask">
				<div class="col-xs-2">
					
				</div>
				<div class="col-xs-4">
					<a href="<?php echo base_url('user/changepassword'); ?>" class="btn btn-default" title="Change Password">Change Password</a>
				</div>
				<div class="col-xs-6">
					
				</div>
			</div>

	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>