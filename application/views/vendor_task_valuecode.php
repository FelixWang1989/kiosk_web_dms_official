<?php $this->load->view('common/header.php'); ?>

            <h4>
                <?php echo strtoupper($vendor_name); ?> - <?php echo "VALUE CODE LICENSE GENERATION"; ?>
            </h4>
            <span><a href="<?php echo base_url('vendortasks').'?vendor='.$vendor_name; ?>" class="pull-right" title="Back">Back</a></span><br>
            <?php if(!empty($result)) : ?>
            <div id="notification_msg" class="alert alert-info">
                <span><b><?php echo $result; ?></b></span>
            </div>
            <?php endif; ?>
            <div class="row vendor-task">
                <div class="col-xs-6 vendor-task-block">
                    <h5>License Generation</h5>
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo current_url().'?vendor='.$vendor_name; ?>">
                        <div class="form-group">
                            <label for="pcmac" class="col-xs-4 control-label">PC MAC: </label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" size="12" maxlength="12" name="pcmac" value="" placeholder="PC MAC">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="port" class="col-xs-4 control-label">PORT: </label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" size="12" maxlength="5" name="port" value="" placeholder="PORT">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-4">
                            </div>
                            <div class="col-xs-8">
                                <input type="submit" class="btn btn-default btn-generate" value="Generate"></input>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12 vendor-task-block">
                            <h5>View Licenses Deployed:</h5>
                            <?php if(!empty($deployed_licenses)): ?>
                            <?php foreach ($deployed_licenses as $deployed_license) : ?>
                            <?php $license = $deployed_license['file'] ?>
                            <?php $license_path = substr($deployed_license['path'],2) ?>
                            <?php $license_port = $deployed_license['port'] ?>
                            <div class="licensedetail">
                                <div><?php echo $license; ?></div>
                                <div class="help-text">
                                    Time Generated: <?php echo date("Y-m-d H:i:s", $generated_times[$license]); ?>
                                </div>
                                <a href="<?php echo base_url().$license_path.'/'.$license; ?>.reg" title="<?php echo $license; ?>">View</a>
                                <a href="<?php echo base_url('vendortasks/download_license').'?vendor='.$vendor_name.'&filename='.$license.'&port='.$license_port; ?>" title="Download">Download</a>
                            </div>
                            <?php endforeach; ?>
                            <?php else : ?>
                            <p><b>No Licenses Deployed Yet.</b></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        <script>
            $('input[type="submit"]').bind('click', function() {
                var pcmac 	= $('input[name="pcmac"]').val();
                var port 	= $('input[name="port"]').val();

                if (pcmac == '') {
                    alert('PC MAC cannot be empty.');
                    $('input[name="pcmac"]').focus();

                    return false;
                };
                if (port == '' || port == 0) {
                    alert('Port error.');
                    $('input[name="port"]').focus();

                    return false;
                };
                if (port >= 65535) {
                    alert('Port must Less than 65535.');
                    $('input[name="port"]').focus();

                    return false;
                };

                return true;
            });

       //  	$(document).ready(function() {
       //  		$('input[name="pcmac"]').iMask({
                //       type      : 'fixed'
                //     , mask      : 'xx:xx:xx:xx:xx:xx'
                //     , stripMask : true
                // });
       //  	});
        </script>

<?php $this->load->view('common/footer.php'); ?>