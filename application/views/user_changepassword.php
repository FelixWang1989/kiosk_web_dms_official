<?php $this->load->view('common/header.php'); ?>

<?php 

/**
 * View: Change Password page for logged in users.
 * @author Lyon Sun <lyonsun@techtrex.com>
 */

 ?>

			<h4>Change Password</h4><span><a href="index" class="pull-right" title="Back">Back</a></span><br>
			<?php if(!empty($result)) : ?>
				<div id="notification_msg" class="alert alert-info">
					<span><b><?php echo $result; ?></b></span>
				</div>
			<?php endif; ?>
			<br>
			<form class="form-horizontal" role="form" method="POST" action="<?php echo current_url(); ?>">
				<div class="form-group">
					<label for="currentpass" class="col-xs-5 control-label">Current Password</label>
					<div class="col-xs-4">
						<input type="password" class="form-control" size="50" name="currentpass" value="" placeholder="Current Password">
					</div>
				</div>

				<div class="form-group">
					<label for="newpass" class="col-xs-5 control-label">New Password</label>
					<div class="col-xs-4">
						<input type="password" class="form-control" size="50" name="newpass" value="" placeholder="New Password">
					</div>
				</div>

				<div class="form-group">
					<label for="repeatnew" class="col-xs-5 control-label">Repeat New Password</label>
					<div class="col-xs-4">
						<input type="password" class="form-control" size="50" name="repeatnew" value="" placeholder="Repeat New Password">
					</div>
				</div>
				
				
				<div class="form-group">
					<div class="col-xs-5">
					</div>
					<div class="col-xs-4">
				    	<input type="submit" class="btn btn-default btn-create" value="Submit"></input>
				    	<a href="index" title="Cancel">Cancel</a>
					</div>
				</div>
			</form>

	    <script>
	    	$('input[type="submit"]').bind('click', function() {
	    		var currentpass	= $('input[name="currentpass"]').val();
	    		var newpass		= $('input[name="newpass"]').val();
	    		var repeatnew	= $('input[name="repeatnew"]').val();

	    		if (currentpass == '') {
	    			alert('Please enter your current password first.');
	    			$('input[name="currentpass"]').focus();
	    			return false;
	    		};

	    		if (newpass == '') {
	    			alert('Please enter your new password.');
	    			$('input[name="newpass"]').focus();
	    			return false;
	    		};

	    		if (repeatnew == '') {
	    			alert('Please repeat your new password.');
	    			$('input[name="repeatnew"]').focus();
	    			return false;
	    		};

	    		if (newpass.length < 5) {
	    			alert('Password must be at least 5 characters long.');
	    			$('input[name="newpass"]').focus();
	    			return false;
	    		};

	    		if (newpass == currentpass) {
	    			alert('Please give a different password.');
	    			$('input[name="newpass"]').focus();
	    			return false;
	    		};

	    		if (repeatnew != newpass) {
	    			alert('New password given doesn\'t match.');
	    			$('input[name="repeatnew"]').focus();
	    			return false;
	    		};

	    		return true;
	    	});
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>