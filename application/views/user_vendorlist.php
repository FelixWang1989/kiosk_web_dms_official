<?php $this->load->view('common/header.php'); ?>

<?php 

/**
 * View: vendor list page for logged in users.
 * @author Lyon Sun <lyonsun@techtrex.com>
 */

 ?>

			<h4>Vendor List</h4>
			<span><a href="index" class="pull-right" title="Back">Back</a></span><br><br>
			<input type="text" class="pull-right" name="search" value="" placeholder="Search">
			
			<?php 
				echo $table == "Undefined table data" ? "<div class='notabledata'>No Data</div> " : $table;
			 ?>

	    <script>
	    	var $rows = $('.table tr');
			$('input[name="search"]').keyup(function() {
			    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

			    $rows.show().filter(function() {
			        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			        return !~text.indexOf(val);
			    }).hide();
			});

	    </script>
			
<?php $this->load->view('common/footer.php'); ?>