<?php $this->load->view('common/header.php'); ?>

<?php 

/**
 * View: vendor tasks page for logged in users.
 * @author Lyon Sun <lyonsun@techtrex.com>
 */

 ?>

			<h4>VENDOR - <?php echo strtoupper($vendor_name); ?></h4>
			<span><a href="user/vendorlist" class="pull-right" title="Back">Back</a></span><br>
			<p>Please select from an option below:</p>
			
			<div class="row vendor-task">
				<div class="col-xs-5">
					<a href="<?php echo current_url(); ?>/lcms?vendor=<?php echo $vendor_name; ?>" class="btn btn-default" title="LCMS License Generation">LCMS License Generation</a>
				</div>
				<div class="col-xs-7 help-text">
					This feature allows you to generate license for LCMS for the vendor, generate target vendor key file for LCMS and view existing licenses for the vendor.
				</div>
			</div>

			<div class="row vendor-task">
				<div class="col-xs-5">
					<a href="<?php echo current_url(); ?>/valuecode?vendor=<?php echo $vendor_name; ?>" class="btn btn-default" title="Value Code License Generation">Value Code License Generation</a>
				</div>
				<div class="col-xs-7 help-text">
					This feature allows you to generate license for value code, target vendor key file for value code and view existing licenses for the vendor.
				</div>
			</div>

			<div class="row vendor-task">
				<div class="col-xs-5">
					<a href="<?php echo current_url(); ?>/dpvc?vendor=<?php echo $vendor_name; ?>" class="btn btn-default" title="Data Preparation – Value Card">Data Preparation – Value Card</a>
				</div>
				<div class="col-xs-7 help-text">
					This feature allows you to generate card data for value cards and view generated card numbers.
				</div>
			</div>

			<div class="row vendor-task">
				<div class="col-xs-5">
					<a href="<?php echo current_url(); ?>/dkcg?vendor=<?php echo $vendor_name; ?>" class="btn btn-default" title="Deployment Key Card Generation">Deployment Key Card Generation</a>
				</div>
				<div class="col-xs-7 help-text">
					This feature allows you to generate deployment key card and view deployed PTS terminals and LMCs.
				</div>
			</div>

	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>