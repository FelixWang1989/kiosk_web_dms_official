<?php $this->load->view('common/header.php'); ?>

			<form class="form-horizontal" action="javascript:">
		        <h2 class="form-signin-heading">Please sign in</h2><br>
		        <div class="form-group">
    		        <label for="username" class="control-label col-lg-4">Enter Your Username</label>
    		        <div class="col-lg-4">
    		        	<input name="username" type="text" class="form-control" placeholder="User Name" autofocus>
    		        </div>
		        </div>
		        <div class="form-group">
		        	<label for="password" class="control-label col-lg-4">Password</label>
    		        <div class="col-lg-4">
    		        	<input name="password" type="password" class="form-control" placeholder="Password">
    		        </div>
    		    </div>
		        <!-- <label class="checkbox">
		          <input type="checkbox" value="remember-me"> Remember me
		        </label> -->
		        <div class="form-group">
		        	<div class="col-lg-offset-4 col-lg-4">
		        		<button class="btn btn-default btn-block btn-login" type="submit">Sign in</button>
		        	</div>
		        </div>
	      	</form>

	    <script>
	    	$('.btn-login').click(function () {
	    		if ($('input[name=username]').val() == 'admin' && $('input[name=password]').val() == 'admin') {
	    			location.href = "user/index";
	    		} else {
	    			location.href = 'user/login_error';
	    		}
	    	})
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>