<?php $this->load->view('common/header.php'); ?>

			<h4>Import Original Reg File</h4>
			<p>Are you sure you want to import original reg file?</p>
			<p><b>CAUTION: This might overwrite your previous creation/generation if any.</b></p>
			<a href="<?php echo base_url('user/do_importregfile'); ?>" class="btn btn-default" title="Continue">Yes, Continue</a>
			<a href="index" title="Cancel">Cancel</a>
	    <script>
	    	
	    </script>
			
<?php $this->load->view('common/footer.php'); ?>